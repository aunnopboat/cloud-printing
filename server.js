const express = require('express');
const app = express();
const cors = require("cors");
const bodyParser = require('body-parser');
const crawler = require('crawler-request');

const port = 5000;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

function respond_page (res) {
  var setPage = res.text.match(/\n\n/g);
  res["totalPage"] = setPage.length;
  var file = res;
  return file;
}

app.listen(port, function () {
  console.log(`Server running on port ${port}`)
});

app.get('/starts', function (req, res) {
  res.json("Back-End side started.")
})

app.post('/pdf-title', function (req, res) {
  crawler(req.body.link, respond_page)
  .then(function (file) {
    res.json(file)
  })
  .catch(function (error) {
    return res.json(error)
  });
})