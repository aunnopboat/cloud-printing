export function costs (price, total_page, quantity) {
  return ((price * total_page) * quantity)
}

export function star (score, max_star) {
  return score >= 0 ? ((score * 100) / max_star ).toFixed(2) : '0.00'
}

export function emoji (score) { 
  if (parseFloat(score) > 4.00) {
    return {value: 'Excellence', emoji: '😁'};
  } else if (parseFloat(score) > 3.00) {
    return {value: 'Good', emoji: '😊'};
  } else if (parseFloat(score) > 2.00) {
    return {value: 'Fair', emoji: '😐'};
  } else if (parseFloat(score) > 1.00) {
    return {value: 'Poor', emoji: '🙁'};
  } else if (parseFloat(score) >= 0.00) {
    return {value: 'Very poor', emoji: '😩'};
  } else {
    return {value: '...', emoji: '😶'};
  }
}