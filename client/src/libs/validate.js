export function isEmail (email = String()) {
  // eslint-disable-next-line
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export function passIsvalid (password = String()) {
  return (password.length > 5)
}

export function telIsvalid (tel = String()) {
  var secondDigit = ['6', '8', '9'];
  if (tel.charAt(0) === '0') {
    if (secondDigit.indexOf(tel.charAt(1)) !== -1) {
      if (tel.length === 10) {
        return true;
      }
    }
  }
  return false;
}

export function equals (value1 = String(), value2 = String()) {
  if (value1 && value2) {
    if (value1 !== String() && value2 !== String()) {
      return (value1 === value2);
    }
  }
  return false
}

export function equalsIgnoreCase (value1 = String(), value2 = String()) {
  if (value1 && value2) {
    if (value1 !== String() && value2 !== String()) {
      return (value1.toLocaleLowerCase() === value2.toLocaleLowerCase());
    }
  }
  return false
}

export function equalsNumber (value1 = String(), value2 = String()) {
  if (value1 !== String() && value1 !== String()) {
    return (parseFloat(value1) === parseFloat(value2));
  }
  return false
}

// eslint-disable-next-line
export function equalsInSet (value = String(), array = Array()) {
  return (array.indexOf(value) !== -1);
}

export function notEmpty (value = String()) {
  return (value !== String());
}

