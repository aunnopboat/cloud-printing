// __tests__/
import { onlyNumber } from '../../filter';

describe('Test get only number from text', () => {
  it('onlyNumber in case [ Test the case that get only number in input ] value "1"', () => {
    const value = "1";
    expect(onlyNumber(value)).toBe("1");
  })
})

describe('Test get only number from text', () => {
  it('onlyNumber in case [ Test the case that get only number in input ] value "a1ee4"', () => {
    const value = "a1ee4";
    expect(onlyNumber(value)).toBe("14");
  })
})

describe('Test get only number from text', () => {
  it('onlyNumber in case [ Test the case that get only number in input ] value "abcd"', () => {
    const value = "abcd";
    expect(onlyNumber(value)).toBe("");
  })
})