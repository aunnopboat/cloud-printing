// __test__/
import { equalsInSet } from '../../validate';

describe('Test input is equals in array element', () => {
  it('equalsInSet in case [ Test the case that first input String is has in array and second input Array ] value 1 "a"  value 2 ["a","b","c","d"]', () => {
    const value1 = "a";
    const value2 = ["a","b","c","d"];
    expect(equalsInSet(value1, value2)).toBe(true);
  })
})

describe('Test input is equals in array element', () => {
  it('equalsInSet in case [ Test the case that first input number is has in array and second input Array ] value 1 2  value 2 [1,2,3,4]', () => {
    const value1 = 2;
    const value2 = [1,2,3,4];
    expect(equalsInSet(value1, value2)).toBe(true);
  })
})

describe('Test input is equals in array element', () => {
  it('equalsInSet in case [ Test the case that first input String is not has in array and second input Array ] value 1 "e"  value 2 ["a","b","c","d"]', () => {
    const value1 = "e";
    const value2 = ["a","b","c","d"];
    expect(equalsInSet(value1, value2)).toBe(false);
  })
})

describe('Test input is equals in array element', () => {
  it('equalsInSet in case [ Test the case that first input number is not has in array and second input Array ] value 1 5  value 2 [1,2,3,4]', () => {
    const value1 = 5;
    const value2 = [1,2,3,4];
    expect(equalsInSet(value1, value2)).toBe(false);
  })
})