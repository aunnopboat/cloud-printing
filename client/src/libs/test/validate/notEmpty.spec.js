// __test__/
import { notEmpty } from '../../validate';

describe('Test input is not empty', () => {
  it('notEmpty in case [ Test the case that input String is empty ] value ""', () => {
    const value = "";
    expect(notEmpty(value)).toBe(false);
  })
})

describe('Test input is not empty', () => {
  it('notEmpty in case [ Test the case that input String is not empty ] value "a"', () => {
    const value = "a";
    expect(notEmpty(value)).toBe(true);
  })
})