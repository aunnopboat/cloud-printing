// __tests__/
import { passIsvalid } from '../../validate';

describe('Test input is in password pattern', () => {
  it('passIsvalid in case [ Test the case that input less than 6 digit ] value ["12345"]', () => {
    const value = "12345";
    expect(passIsvalid(value)).toBe(false);
  })
})

describe('Test input is in password pattern', () => {
  it('passIsvalid in case [ Test the case that input equal 6 digit ] value ["123456"]', () => {
    const value = "123456";
    expect(passIsvalid(value)).toBe(true);
  })
})

describe('Test input is in password pattern', () => {
  it('passIsvalid in case [ Test the case that input more than 6 digit ] value ["1234567"]', () => {
    const value = "1234567";
    expect(passIsvalid(value)).toBe(true);
  })
})