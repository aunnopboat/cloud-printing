// __test__/
import { equalsNumber } from '../../validate';

describe('Test two number input is equals', () => {
  it('equalsNumber in case [ Test the case that two input number is the same ] value 1 1  value 2 1', () => {
    const value1 = 1;
    const value2 = 1;
    expect(equalsNumber(value1, value2)).toBe(true);
  })
})

describe('Test two number input is equals', () => {
  it('equalsNumber in case [ Test the case that two input number is not the same ] value 1 1  value 2 2', () => {
    const value1 = 1;
    const value2 = 2;
    expect(equalsNumber(value1, value2)).toBe(false);
  })
})

describe('Test two number input is equals', () => {
  it('equalsNumber in case [ Test the case that first input is empty and second input is empty ] value 1 ""  value 2 ""', () => {
    const value1 = "";
    const value2 = "";
    expect(equalsNumber(value1, value2)).toBe(false);
  })
})