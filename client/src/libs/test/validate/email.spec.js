// __tests__/
import { isEmail } from '../../validate';

describe('Test input is in email pattern', () => {
  it('isEmail in case [ Test the case that input normal String ] value "example"', () => {
    const value = "example";
    expect(isEmail(value)).toBe(false);
  })
})

describe('Test input is in email pattern', () => {
  it('isEmail in case [ Test the case that input normal number ] value 1234', () => {
    const value = 1234;
    expect(isEmail(value)).toBe(false);
  })
})

describe('Test input is in email pattern', () => {
  it('isEmail in case [ Test the case that input normal number and String but not in email form ] value "example1234"', () => {
    const value = `example${1234}`;
    expect(isEmail(value)).toBe(false);
  })
})

describe('Test input is in email pattern', () => {
  it('isEmail in case [ Test the case that input String in email form ] value "example@email.com"', () => {
    const value = "example@email.com";
    expect(isEmail(value)).toBe(true);
  })
})

describe('Test input is in email pattern', () => {
  it('isEmail in case [ Test the case that input number in email form ] value "1234@email.com"', () => {
    const value = `${1234}@email.com`;
    expect(isEmail(value)).toBe(true);
  })
})

describe('Test input is in email pattern', () => {
  it('isEmail in case [ Test the case that input String and number in email form ] value "example1234@email.com"', () => {
    const value = `example${1234}@email.com`;
    expect(isEmail(value)).toBe(true);
  })
})
