// __tests__/
import { telIsvalid } from '../../validate';

describe('Test input is in telphone number pattern', () => {
  it('passIsvalid in case [ Test the case that input less than 10 digit not in telphone number form ] value ["123456789"]', () => {
    const value = "123456789";
    expect(telIsvalid(value)).toBe(false);
  })
})

describe('Test input is in telphone number pattern', () => {
  it('passIsvalid in case [ Test the case that input equal 10 digit not in telphone number form ] value ["1234567890"]', () => {
    const value = "1234567890";
    expect(telIsvalid(value)).toBe(false);
  })
})

describe('Test input is in telphone number pattern', () => {
  it('passIsvalid in case [ Test the case that input more than 10 digit not in telphone number form ] value ["12345678910"]', () => {
    const value = "12345678910";
    expect(telIsvalid(value)).toBe(false);
  })
})

describe('Test input is in telphone number pattern', () => {
  it('passIsvalid in case [ Test the case that input less than 10 digit in telphone number form ] value ["080123456"]', () => {
    const value = "080123456";
    expect(telIsvalid(value)).toBe(false);
  })
})

describe('Test input is in telphone number pattern', () => {
  it('passIsvalid in case [ Test the case that input equal 10 digit in telphone number form ] value ["0801234567"]', () => {
    const value = "0801234567";
    expect(telIsvalid(value)).toBe(true);
  })
})

describe('Test input is in telphone number pattern', () => {
  it('passIsvalid in case [ Test the case that input more than 10 digit in telphone number form ] value ["08012345678"]', () => {
    const value = "08012345678";
    expect(telIsvalid(value)).toBe(false);
  })
})