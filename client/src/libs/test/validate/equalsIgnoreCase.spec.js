// __test__/
import { equalsIgnoreCase } from '../../validate';

describe('Test two input is equalsIgnoreCase', () => {
  it('equalsIgnoreCase in case [ Test the case that two input String is the same in upper case ] value 1 "ABCD"  value 2 "ABCD"', () => {
    const value1 = "ABCD";
    const value2 = "ABCD";
    expect(equalsIgnoreCase(value1, value2)).toBe(true);
  })
})

describe('Test two input is equalsIgnoreCase', () => {
  it('equalsIgnoreCase in case [ Test the case that two input String is the same in lower case ] value 1 "abcd" value 2 "abcd"', () => {
    const value1 = "abcd";
    const value2 = "abcd";
    expect(equalsIgnoreCase(value1, value2)).toBe(true);
  })
})

describe('Test two input is equalsIgnoreCase', () => {
  it('equalsIgnoreCase in case [ Test the case that two input String is the same ] value 1 "abCD" value 2 "abCD"', () => {
    const value1 = "abCD";
    const value2 = "abCD";
    expect(equalsIgnoreCase(value1, value2)).toBe(true);
  })
})

describe('Test two input is equalsIgnoreCase', () => {
  it('equalsIgnoreCase in case [ Test the case that first input String in upper case and second input String in lower case is the same ] value 1 "ABCD" value 2 "abcd"', () => {
    const value1 = "ABCD";
    const value2 = "abcd";
    expect(equalsIgnoreCase(value1, value2)).toBe(true);
  })
})

describe('Test two input is equalsIgnoreCase', () => {
  it('equalsIgnoreCase in case [ Test the case that first input String and second input String is the same but in different case ] value 1 "AbCd" value 2 "aBcD"', () => {
    const value1 = "AbCd";
    const value2 = "aBcD";
    expect(equalsIgnoreCase(value1, value2)).toBe(true);
  })
})

describe('Test two input is equalsIgnoreCase', () => {
  it('equalsIgnoreCase in case [ Test the case that first input String and second input String is not the same ] value 1 "abcd" value 2 "efgh"', () => {
    const value1 = "abcd";
    const value2 = "efgh";
    expect(equalsIgnoreCase(value1, value2)).toBe(false);
  })
})

describe('Test two input is equalsIgnoreCase', () => {
  it('equalsIgnoreCase in case [ Test the case that first input is empty and second input is empty ] value 1 "" value 2 ""', () => {
    const value1 = "";
    const value2 = "";
    expect(equalsIgnoreCase(value1, value2)).toBe(false);
  })
})