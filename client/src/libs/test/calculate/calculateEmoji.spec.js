import { emoji } from '../../calculate';

describe('Test calculate emoji', () => {
  it('emoji in case [ Test the case that input value ]', () => {
    expect(emoji(0.00).value).toBe('Very poor');
  })
})

describe('Test calculate emoji', () => {
  it('emoji in case [ Test the case that input value ]', () => {
    expect(emoji(1.01).value).toBe('Poor');
  })
})

describe('Test calculate emoji', () => {
  it('emoji in case [ Test the case that input value ]', () => {
    expect(emoji(2.01).value).toBe('Fair');
  })
})

describe('Test calculate emoji', () => {
  it('emoji in case [ Test the case that input value ]', () => {
    expect(emoji(3.01).value).toBe('Good');
  })
})

describe('Test calculate star', () => {
  it('emoji in case [ Test the case that input value ]', () => {
    expect(emoji(4.01).value).toBe('Excellence');
  })
})