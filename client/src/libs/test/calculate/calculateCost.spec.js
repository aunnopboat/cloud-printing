import { costs } from '../../calculate';

describe('Test calculate cost', () => {
  it('costs in case [ Test the case that input value ]', () => {
    const price = 1, total_page = 2, quantity = 4;
    expect(costs(price, total_page, quantity)).toBe(10);
  })
})

describe('Test calculate cost', () => {
  it('costs in case [ Test the case that input value ]', () => {
    const price = 10, total_page = 4, quantity = 4;
    expect(costs(price, total_page, quantity)).toBe(160);
  })
})