import { star } from '../../calculate';

describe('Test calculate star', () => {
  it('star in case [ Test the case that input value ]', () => {
    const score = 4, max_star = 5;
    expect(star(score, max_star)).toBe('80.00');
  })
})

describe('Test calculate star', () => {
  it('star in case [ Test the case that input value ]', () => {
    const score = 4.5, max_star = 5;
    expect(star(score, max_star)).toBe('90.00');
  })
})

describe('Test calculate star', () => {
  it('star in case [ Test the case that input value ]', () => {
    const score = -1, max_star = 5;
    expect(star(score, max_star)).toBe('0.00');
  })
})