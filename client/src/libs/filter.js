export function onlyNumber (value = String()) {
  if (value !== String()) {
    // eslint-disable-next-line
    return (value.replace(/[^0-9\.]+/g, String()));
  }
  return String();
}

export function filterName (word = String(), object = {}) {
  var filter = object.filter(data => {
    return (
      (data.name.toLowerCase().indexOf(word.toLowerCase()) !== -1) && 
      (data.name.toLowerCase().charAt(0) === word.toLowerCase().charAt(0))
    );
  })
  return filter;
}