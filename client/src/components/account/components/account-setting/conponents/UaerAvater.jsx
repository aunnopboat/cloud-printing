import React, { Component } from 'react';
import { AVATAR } from '../../../../../resources/images/user-avatar/avatar-item';
import { FaPencilAlt } from 'react-icons/fa';
import * as validate from '../../../../../libs/validate';
import firebase from 'firebase';
import swal from 'sweetalert';
import Modal from 'react-bootstrap/Modal';

class UserAvatar extends Component {
  constructor(props){
    super(props)
    // eslint-disable-next-line
    this.avatarIndex = Array();
    this.state = {
      uid: this.props.uid,
      picture: this.props.picture,
      pic_select: this.props.picture,
      open: false
    }
  }

  componentDidMount() {
    this.getAvatarIndex()
  }

  getAvatarIndex() {
    for (var i = 1; i < AVATAR.length; i++) {
      this.avatarIndex.push(AVATAR[i]);
    }
  }
  
  handleClick = (event) => {
    this.setState({pic_select: parseInt(event.target.name)});
  }

  // set user avater ตามที่ user select
  saveAvatar = () => {
    firebase.database().ref().child('users').child(this.state.uid).update({
      picture: this.state.pic_select
    }).then(() => {
      swal({
        title: "Change Avatar",
        icon: "success",
        timer: 2000
      }).then(() => {
        this.setState({open: false}, () => {
          window.location.reload();
        });
      });
    });
  }

  render() {
    const { open, pic_select } = this.state;
    return (
      <div className="avatar-container d-flex justify-content-center">
        <div className="image-wraper" onClick={this.openModal}>
          <img className="avatar-border" alt="user avatar" src={AVATAR[this.state.picture]}/>
          <p className="edit-avatar"><FaPencilAlt/> Edit Avatar</p>
        </div>
        <Modal show={open} onHide={this.closeModal}>
          <div className="modal-bg">
            <Modal.Header>
              <Modal.Title style={{color: 'white'}}><strong>Avatar List</strong></Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-bg">
              <div className="row d-flex justify-content-center">
              {
                this.avatarIndex.map((image, key) => {
                  return (
                    <div key={key} align="center" className="col-12 col-md-6 mt-2 mb-2">
                      <img alt="avatar-img" src={image} onClick={this.handleClick}
                        style={{opacity: validate.equals(pic_select, (key + 1)) ? 1 : 0.6}} name={(key + 1)}
                        className="avatar-list-img d-flex justify-content-center"/>
                    </div>
                  );
                })
              }
              </div>
            </Modal.Body>
            <Modal.Footer>
              <button type="button" className="btn btn-secondary" onClick={this.closeModal}><strong>Close</strong></button>
              <button type="button" className="btn btn-primary" onClick={this.saveAvatar}><strong>Save</strong></button>
            </Modal.Footer>
          </div>
        </Modal>
      </div>
    );
  }

  closeModal = () => {
    this.setState({
      open: false,
      pic_select: this.state.picture
    })
  }

  openModal = () => {
    this.setState({open: true})
  }
}

export default UserAvatar;