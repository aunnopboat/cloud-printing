import React, { Component } from 'react';
import { FaPencilAlt, FaUserEdit } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import * as validate from '../../../../../libs/validate';
import firebase from 'firebase';
import swal from 'sweetalert';
import Modal from 'react-bootstrap/Modal';

class FullNameField extends Component {
  constructor(props){
    super(props)
    this.state = {
      uid: this.props.uid,
      name: this.props.name,
      new_name: this.props.name,
      open: false
    }
  }

  handleChange = (event) => {
    this.setState({[event.target.name]: event.target.value});
  }

  changeName = () => {
    return (!validate.equals(this.state.name, this.state.new_name) && validate.notEmpty(this.state.new_name))
  }

  rederSeveBtn = () => {
    if (!validate.equals(this.state.name, this.state.new_name) &&
      validate.notEmpty(this.state.new_name)) {
      return (
        <button type="button" className="btn btn-primary" onClick={this.editName}><strong>Save</strong></button>
      );
    } else {
      return (
        <button type="button" className="btn btn-primary" disabled><strong>Save</strong></button>
      );
    }
  }

  editName = () => {
    firebase.database().ref().child('users').child(this.state.uid).update({
      name: this.state.new_name
    }).then(() => {
      swal({
        title: "Change Name",
        icon: "success",
        timer: 2000
      }).then(() => {
        this.setState({open: false}, () => {
          window.location.reload();
        });
      });
    });
  }

  render() {
    const { open } = this.state;
    return (
      <div className="setting-item row">
        <span className="setting-title">Full name</span>
        <span className="setting-value">: {this.state.name}</span>
        <span><Link to="#" className="edited" onClick={this.openModal}><FaPencilAlt/> Edit</Link></span>
        <Modal show={open} onHide={this.closeModal}>
          <div className="modal-bg">
            <Modal.Header>
              <Modal.Title style={{color: 'white'}}><strong>Change Name</strong></Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-bg">
              <div className="input-group mb-3 mt-3">
                <div className="input-group-prepend">
                  <span className="input-group-text"><FaUserEdit/> </span>
                </div>
                <input type="text" className={"form-control" + (this.changeName() ? ' is-valid' : String())} 
                  name="new_name" placeholder="Full Name" value={this.state.new_name} onChange={this.handleChange}/>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <button type="button" className="btn btn-secondary" onClick={this.closeModal}><strong>Close</strong></button>
              {this.rederSeveBtn()}
            </Modal.Footer>
          </div>
        </Modal>
      </div>
    );
  }

  closeModal = () => {
    this.setState({
      open: false,
      new_name: this.state.name
    })
  }

  openModal = () => {
    this.setState({open: true})
  }
}

export default FullNameField;