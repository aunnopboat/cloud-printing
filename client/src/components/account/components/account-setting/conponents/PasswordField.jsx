import React, { Component } from 'react';
import { Base64 } from 'js-base64';
import { FaPencilAlt, FaKey, FaExchangeAlt, FaMedapps } from 'react-icons/fa';
import * as validate from '../../../../../libs/validate';
import firebase from 'firebase';
import swal from 'sweetalert';
import Modal from 'react-bootstrap/Modal';

class PasswordField extends Component {
  constructor(props){
    super(props)
    this.state = {
      pass_view: false,
      password: this.props.password,
      old_password: String(),
      new_password: String(),
      confirm_password: String(),
      open: false
    }
  }

  // double click เพื่อ set type ของ input field
  handleDoubleClick = (event) => {
    switch (event.target.type) {
      case 'password': event.target.type = 'text'; break;
      case 'text': event.target.type = 'password'; break;
      default: break;
    }
  }

  handleChange = (event) => {
    this.setState({[event.target.name]: event.target.value});
  }

  // เปลี่ยน password ของ user
  changePassword = () => {
    swal({
      title: "Are you sure?",
      text: 'Click "OK" if you want to change pasword.',
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((change) => {
      if (change) {
        firebase.auth().currentUser.updatePassword(Base64.encode(this.state.new_password))
        .then(() => {
          firebase.database().ref().child('users').child(this.props.uid).update({
            password: Base64.encode(this.state.new_password)
          }).then(() => {
            swal({
              title: "Your password has already changed!!",
              icon: "success",
              timer: 2000
            }).then(() => {
              firebase.auth().signOut().then(() => {
                window.location.replace('/login');
              });
            });
          }).catch((error) => {
            swal({
              title: "Error!!",
              text: `${error} Please SignOut and SignIn again.`,
              icon: "error",
              timer: 3000
            });
          })
        }).catch((error) => {
          swal({
            title: "Error!!",
            text: `${error} Please SignOut and SignIn again.`,
            icon: "error",
            timer: 3000
          });
        })
      } else {
        swal({
          title: "Your current password is now safe!!",
          timer: 2000
        })
        .then(() => {
          this.closeModal();
        });
      }
    });
  }

  rederChangePassBtn = () => {
    if (!validate.equals(this.state.password, Base64.encode(this.state.new_password)) &&
      validate.equals(Base64.encode(this.state.old_password), this.state.password) &&
      validate.equals(this.state.confirm_password, this.state.new_password) &&
      validate.passIsvalid(this.state.new_password)) {
      return (
        <button type="button" className="btn btn-primary" onClick={this.changePassword}><strong>Change</strong></button>
      );
    } else {
      return (
        <button type="button" className="btn btn-primary" disabled><strong>Change</strong></button>
      );
    }
  }

  render() {
    const { open } = this.state;
    return (
      <div className="setting-item row">
        <span className="setting-title">Password</span>
        <span className="mr-1">: </span>
        <span className="change-password" onClick={this.openModal}><FaPencilAlt className="change-password-icon mr-1"/>Change Password</span>
        <Modal show={open} onHide={this.closeModal}>
          <div className="modal-bg">
            <Modal.Header>
              <Modal.Title style={{color: 'white'}}><strong>Change Password</strong></Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-bg">
              <div className="input-group mb-3 mt-3" data-toggle="tooltip" title="Must have 6 characters or more than">
                <div className="input-group-prepend">
                  <span className="input-group-text"><FaKey/> </span>
                </div>
                <input type="password" className={"form-control" + (validate.equals(Base64.encode(this.state.old_password), this.state.password) ? ' is-valid' : String())} 
                  name="old_password" placeholder="Current Password" value={this.state.old_password} onChange={this.handleChange} onDoubleClick={this.handleDoubleClick}/>
              </div>
              <div className="input-group mb-3 mt-3" data-toggle="tooltip" title="Must have 6 characters or more than">
                <div className="input-group-prepend">
                  <span className="input-group-text"><FaExchangeAlt/> </span>
                </div>
                <input type="password" className={"form-control" + (validate.passIsvalid(this.state.new_password) && !validate.equals(this.state.password, Base64.encode(this.state.new_password)) ? ' is-valid' : String())} 
                  name="new_password" placeholder="New Password" value={this.state.new_password} onChange={this.handleChange} onDoubleClick={this.handleDoubleClick}/>
              </div>
              <div className="input-group mb-3 mt-3" data-toggle="tooltip" title="Must have 6 characters or more than">
                <div className="input-group-prepend">
                  <span className="input-group-text"><FaExchangeAlt/> </span>
                </div>
                <input type="password" className={"form-control" + (validate.equals(this.state.confirm_password, this.state.new_password) ? ' is-valid' : String())} 
                  name="confirm_password" placeholder="Confirm New Password" value={this.state.confirm_password} onChange={this.handleChange} onDoubleClick={this.handleDoubleClick}/>
              </div>
              <span className="warning"><FaMedapps className="mr-2"/>Double click for seeing input value.</span>
            </Modal.Body>
            <Modal.Footer>
              <button type="button" className="btn btn-secondary" onClick={this.closeModal}><strong>Close</strong></button>
              {this.rederChangePassBtn()}
            </Modal.Footer>
          </div>
        </Modal>
      </div>
    );
  }

  closeModal = () => {
    this.setState({
      open: false,
      old_password: String(),
      new_password: String(),
      confirm_password: String()
    })
  }

  openModal = () => {
    this.setState({open: true})
  }
}

export default PasswordField;