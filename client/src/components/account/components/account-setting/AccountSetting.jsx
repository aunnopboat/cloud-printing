import React, { Component } from 'react';
import firebase from 'firebase';

import UserAvater from './conponents/UaerAvater.jsx';
import FullName from './conponents/FullNameField.jsx';
import Password from './conponents/PasswordField.jsx';

class AccountSetting extends Component {
  constructor(props){
    super(props)
    this.state = {
      user: null,
      name: null,
      email: null,
      tel: null,
      username: null,
      type: null,
      password: null,
      picture: null,
    }
  }

  componentDidMount = () => {
    this.getUser();
  }

  getUser() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({user: user}, () => {
          firebase.database().ref().child('users').child(this.state.user.uid).once('value')
          .then((snap) => {
            if (snap.val()) {
              this.setState({
                name: snap.val().name,
                email: snap.val().email,
                tel: snap.val().tel,
                username: snap.val().username,
                password: snap.val().password,
                picture: parseInt(snap.val().picture),
                type: snap.val().user_type
              });
            }
          });
        });
      }
    });
  }

  renderUserAvatar = () => {
    return (
      <div>
        <h4 className="setting-head mb-4"><strong>User Information</strong></h4>
        {
          this.state.picture ?
          (
            <UserAvater
              picture={this.state.picture}
              uid={this.state.user.uid}
            />
          ) : 
          (null)
        }
      </div>
    );
  }

  renderUserDetail = () => {
    if (this.state.name && this.state.password) {
      return (
        <div className="setting-item-container">
          <FullName
            name={this.state.name}
            uid={this.state.user.uid}
          />
          <div className="setting-item row">
            <span className="setting-title">Email</span>
            <span className="setting-value">: {this.state.email}</span>
          </div>
          <div className="setting-item row">
            <span className="setting-title">Telephone</span>
            <span className="setting-value">: {this.state.tel}</span>
          </div>
          <div className="setting-item row">
            <span className="setting-title">User type</span>
            <span className="setting-value">: {this.state.type}</span>
          </div>
          <div className="setting-item row">
            <span className="setting-title">Username</span>
            <span className="setting-value">: {this.state.username}</span>
          </div>
          <Password
            password={this.state.password}
            uid={this.state.user.uid}
          />
        </div>
      );
    }
  }

  render() {
    return (
      <div className="account-setting" style={{display: this.props.status}}> 
        {this.renderUserAvatar()}
        {this.renderUserDetail()}
      </div>
    );
  }
}

export default AccountSetting;