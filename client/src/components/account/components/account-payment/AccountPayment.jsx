import React, { Component } from 'react';

import PaymentHistoryList from './components/PaymentHistoryList.jsx';

class AccountPayment extends Component {
  constructor(props){
    super(props)
    this.state = {
      historys: this.props.historys
    }
  }

  render() {
    return (
      <div className="account-payment" style={{display: this.props.status}}>
        <h4 className="setting-head mb-4"><strong>Payment</strong></h4>
        <div className="credit-area">
          {
            this.state.historys ? 
            (<PaymentHistoryList historys={this.state.historys} />) : 
            (null)
          }
        </div>
      </div>
    );
  }
}

export default AccountPayment;