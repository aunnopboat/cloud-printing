import React, { Component } from 'react';

import List from './PaymentHistoryItem.jsx';

class PaymentHistory extends Component {
  constructor(props){
    super(props)
    this.state = {
      historys: this.props.historys,
    }
  }

  render() {
    return (
      <div className="history-container">
        <h5 className="mb-3"><strong>Payment history</strong></h5>
        <div className="history-border-head">
          <div className="history-item row">
            <div className="col-sm-3">Date</div>
            <div className="col-sm-3">Status</div>
            <div className="col-sm-3">Service Name</div>
            <div className="col-sm-3">Point</div>
          </div>
        </div>
        <div className="history-length">
          {
            this.state.historys ? 
            (
              this.state.historys.length > 0 ?
              (
                this.state.historys.map((history, key) => {
                  return (
                    <List key={key} history={history}/>
                  );
                })
              ) : 
              (null)
            ) : 
            (null)
          }
      </div>
      </div>
    );
  }
}

export default PaymentHistory;