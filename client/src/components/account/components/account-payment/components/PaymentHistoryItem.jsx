import React, { Component } from 'react';
import firebase from 'firebase';

class PaymentHistoryItem extends Component {
  constructor(props){
    super(props)
    this.state = {
      history: this.props.history,
      name: null
    }
  }

  componentDidMount() {
    this.getName()
  }

  getName = () => {
    const { history } = this.state
    firebase.database().ref().child('services').child(history.service_id)
    .once('value').then((snap) => {
      this.setState({name: snap.val().name})
    })
  }

  render() {
    const { history, name } = this.state
    return (
      <div className="history-border">
        <div className="history-item row">
          <div className="col-sm-3">{new Date(parseInt(history.time)).toLocaleDateString("en-GB")}</div>
          <div className="col-sm-3">Paid</div>
          <div className="col-sm-3">{name ? name : String()}</div>
          <div className="col-sm-3">{history.income} THB</div>
        </div>
      </div>
    );
  }
}

export default PaymentHistoryItem;