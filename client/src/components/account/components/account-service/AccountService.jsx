import React, { Component } from 'react';
import firebase from 'firebase';
import swal from 'sweetalert';
import * as filter from '../../../../libs/filter';
import * as validate from '../../../../libs/validate';

class AccountService extends Component {
  constructor(props) {
    super(props)
    this.ORfile = null;
    this.state = {
      user: null,
      name: String(),
      email: String(),
      address: String(),
      promptpay: String(),
      latitude: String(),
      longitude: String(),
      b_price: 0,
      c_price: 0,
      QRcode: String(),
      account_number: String(),
      bank_name: String(),
      getLat: String(),
      getLong: String(),
      untilUpload: false,
      progress: 0,
    }
  }

  componentDidMount() {
    this.getUser();
    this.getLocation();
  }

  getUser() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({user: user}, () => {
          firebase.database().ref().child('services').child(this.state.user.uid).once('value')
          .then((snap) => {
            if (snap.val()) {
              this.setState({
                name: snap.val().name,
                email: snap.val().email,
                address: snap.val().address,
                promptpay: snap.val().promptpay,
                latitude: snap.val().latitude,
                longitude: snap.val().longitude,
                b_price: parseInt(snap.val().b_price),
                c_price: parseInt(snap.val().c_price),
                QRcode: snap.val().QRcode_link,
                account_number: snap.val().account_number,
                bank_name: snap.val().bank_name
              })
            }
          });
        });
      }
    });
  }

  handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({[name] : value}, () => {
      this.setState({
        promptpay: filter.onlyNumber(this.state.promptpay),
        account_number: filter.onlyNumber(this.state.account_number)
      }, () => {
        if (validate.equalsIgnoreCase(name, 'bank_name') && !validate.notEmpty(value)) {
          this.setState({account_number: String()})
        }
      });
    });
  }

  handlePrice = (event) => {
    var value = filter.onlyNumber(event.target.value)
    if (value <= 0) {
      value = 0
    }
    this.setState({[event.target.name]: parseInt(value)})
  }

  saveEdit = () => {
    if (validate.notEmpty(this.state.bank_name)) {
      if (validate.notEmpty(this.state.account_number)) {
        this.updateService()
      } else {
        swal({
          title: "Please input account number.",
          icon: "warning",
          timer: 2000
        });
      }
    } else {
      this.updateService()
    }
  }

  updateService = () => {
    var update = {
      name: this.state.name,
      email: this.state.email,
      address: this.state.address,
      promptpay: this.state.promptpay,
      latitude: parseFloat(this.state.latitude),
      longitude: parseFloat(this.state.longitude),
      b_price: this.state.b_price,
      c_price: this.state.c_price,
      bank_name: this.state.bank_name,
      account_number: this.state.account_number,
      QRcode_link: this.state.QRcode
    }
    firebase.database().ref().child('services').child(this.state.user.uid).update(update)
    .then(() => {
      swal({
        title: "Update data success",
        icon: "success",
        timer: 2000
      });
    })
  }

  getLocation() {
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(location => {
        this.setState({
          getLat: parseFloat(location.coords.longitude),
          getLong: parseFloat(location.coords.latitude)
        })
      })
    } else {
      swal({
        title: "Geolocation is not supported by this browser.", 
        text: "Cannot get geolocation from this browser.", 
        icon: "warning",
        timer: 2000
      });
    }
  }

  currentLocation = () => {
    const not_empty = (
      validate.notEmpty(this.state.latitude) && 
      validate.notEmpty(this.state.longitude)
    );
    const zero = (
      validate.equalsNumber(this.state.latitude, 0) &&
      validate.equalsNumber(this.state.longitude, 0)
    );
    if (!not_empty || zero) {
      this.setState({
        latitude: this.state.getLat,
        longitude: this.state.getLong
      })
    }
  }

  uploadQR = () => {
    const uploadTask = firebase.storage().ref().child('QRcode').child(this.state.user.uid).put(this.QRfile)
    uploadTask.on('state_changed', (snapshot) => {
      var progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
      this.setUntilUpload(true);
      this.setState({progress: progress})
    }, (error) => {
      swal({
        title: "Failed to upload", 
        text: `${error}`, 
        icon: "error"
      })
    }, () => {
      uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
        this.setUntilUpload(false);
        this.setQRlink(downloadURL)
      });
    });
  }

  setQRlink (link) {
    this.setState({QRcode: link}, () => {
      firebase.database().ref().child('services').child(this.state.user.uid)
      .update({
        QRcode_link: link
      })
    })
  }

  deleteQR = () => {
    swal({
      title: "Are you sure?",
      text: `Remove QR Code`,
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((res) => {
      if (res) {
        firebase.storage().ref().child('QRcode').child(this.state.user.uid)
        .delete().then(() => {
          firebase.database().ref().child('services').child(this.state.user.uid)
          .update({
            QRcode_link: String()
          }).then(() => {
            this.setState({QRcode: String()})
          })
        })
      }
    })
  }

  renderField = () => {
    return (
      <div className="service-content">
        <div className="service-item mt-4 row">
          <div className="col-sm-2 sevice-item-head mt-2">Service name</div>
          <input type="text" name="name" value={this.state.name} onChange={this.handleChange}
            className="col-sm-10 mb-4 form-control"/>
        </div>
        <div className="service-item mt-4 row">
          <div className="col-sm-2 sevice-item-head mt-2">Email</div>
          <input type="text" name="email" value={this.state.email} onChange={this.handleChange}
            className="col-sm-4 mb-4 form-control"/>
            <div className="col-sm-2 sevice-item-head mt-2">Promptpay</div>
          <input type="text" name="promptpay" value={this.state.promptpay} onChange={this.handleChange}
            className="col-sm-4 mb-4 form-control"/>
        </div>
        <div className="service-item mt-4 row">
          <div className="col-sm-2 sevice-item-head mt-3">Address</div>
          <textarea type="text" name="address" value={this.state.address} onChange={this.handleChange}
            className="col-sm-10 mb-4 form-control" autoComplete="off"></textarea>
        </div>
        <div className="service-item mt-4 row">
          <div className="col-sm-2 sevice-item-head mt-2">Latitude</div>
          <input type="number" name="longitude" value={this.state.longitude} onChange={this.handleChange}
            className="col-sm-4 mb-4 form-control" onClick={this.currentLocation}/>
          <div className="col-sm-2 sevice-item-head mt-2">Longitude</div>
          <input type="number" name="latitude" value={this.state.latitude} onChange={this.handleChange}
            className="col-sm-4 mb-4 form-control" onClick={this.currentLocation}/>
        </div>
        <div className="service-item mt-4 row">
          <div className="col-sm-2 sevice-item-head">Mono Price (THB)</div>
          <input type="number" name="b_price" value={this.state.b_price} onChange={this.handlePrice}
            className="col-sm-4 mb-4 form-control"/>
          <div className="col-sm-2 sevice-item-head">Color Price (THB)</div>
          <input type="number" name="c_price" value={this.state.c_price} onChange={this.handlePrice}
            className="col-sm-4 mb-4 form-control"/>
        </div>
        <div className="service-item mt-4 row">
          <div className="col-sm-2 sevice-item-head mt-2">Bank</div>
          <select className="col-sm-3 mb-4 form-control" name="bank_name"
            style={{color: validate.notEmpty(this.state.bank_name) ? 'black' : 'gray'}}
            value={this.state.bank_name} onChange={this.handleChange}>
            <option value="">- Select bank -</option>
            <option value="SCB">SCB</option>
            <option value="K-BANK">K-Bank</option>
          </select>
          <div className="col-sm-3 sevice-item-head mt-2">Account Number</div>
          <input type="text" name="account_number" value={this.state.account_number} onChange={this.handleChange}
            disabled={!validate.notEmpty(this.state.bank_name)} className="col-sm-4 mb-4 form-control"/>
        </div>
        <div className="service-item mt-4 row">
          <div className="col-sm-2 sevice-item-head mt-2 mb-4">QR Code</div>
          {
            validate.notEmpty(this.state.QRcode) ? 
            (
              <div className="col-sm-12 ml-2 mr-2">
                <img alt="QRcode" src={this.state.QRcode} width="100%"/>
                <button type="button" className="clear-img mt-2" onClick={this.deleteQR}>Clear</button>
              </div>
            ) : 
            (
              this.state.untilUpload ?
              (
                <div className="col-sm-4 mb-5 mt-1" 
                  style={{
                    color: validate.equalsNumber(this.state.progress, 100) ? '#4bb14e' : 'yellow',
                    fontSize: '18px'
                  }}>
                  <strong>{this.state.progress} %</strong>
                </div>
              ) : 
              (
                <div className="col-sm-10 mb-4 row">
                  <input type="file" className="col-sm-6 form-control mr-1" onChange={(event) => this.QRfile = event.target.files[0]}/>
                  <button type="button" className="col-sm-2 btn-up-qrcode" onClick={this.uploadQR}>Upload</button>
                </div>
              )
            )
          }
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="account-service" style={{display: this.props.status}}>
        <h4 className="setting-head mb-4"><strong>Service Dashboard</strong></h4>
        <div className="service-border ml-4">
          {this.renderField()}
          <div className="row">
            <div className="service-edit-btn ml-4 mt-3" onClick={this.saveEdit}><strong>Save</strong></div>
          </div>
        </div>
      </div>
    );
  }

  setUntilUpload = (status) => {
    this.setState({untilUpload: status});
  }
}

export default AccountService;