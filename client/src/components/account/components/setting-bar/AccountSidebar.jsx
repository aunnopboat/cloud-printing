import React, { Component } from 'react';
import * as validate from '../../../../libs/validate';
import firebase from 'firebase';

class Switch extends Component {
  constructor(props) {
    super(props)
    this.state = {
      role: null
    }
  }

  componentDidMount = () => {
    this.getUser();
  }

  getUser() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({user: user}, () => {
          firebase.database().ref().child('users').child(this.state.user.uid).once('value')
          .then((snap) => {
            if (snap.val()) {
              this.setState({role: snap.val().user_type});
            }
          })
        });
      }
    });
  }

  renderAccountSetting = () => {
    if (this.state.role) {
      return (
        <div name="info" className="account-sidebar-item" onClick={() => {this.props.setSelect('profile')}} 
          style={{backgroundColor: validate.equalsIgnoreCase(this.props.display[0], 'block') ? '#61666e':'#6c737c', 
          fontWeight: validate.equalsIgnoreCase(this.props.display[0], 'block') ? 'bold':'normal',
          textDecoration: validate.equalsIgnoreCase(this.props.display[0], 'block') ? 'underline':'none'}}>
          User Infomation
        </div>
      );
    }
  }

  renderUserSetting = () => {
    if (validate.equalsIgnoreCase(this.state.role, 'USER')) {
      return (
        <div name="payment" className="account-sidebar-item" onClick={() => {this.props.setSelect('payment')}}
          style={{backgroundColor: validate.equalsIgnoreCase(this.props.display[1], 'block') ? '#61666e':'#6c737c', 
          fontWeight: validate.equalsIgnoreCase(this.props.display[1], 'block') ? 'bold':'normal',
          textDecoration: validate.equalsIgnoreCase(this.props.display[1], 'block') ? 'underline':'none'}}>
          Payment/Credit
        </div>
      );
    }
  }

  renderServiceSetting = () => {
    if (validate.equalsIgnoreCase(this.state.role, 'SERVICE')) {
      return (
        <div name="service" className="account-sidebar-item" onClick={() => {this.props.setSelect('service')}}
          style={{backgroundColor: validate.equalsIgnoreCase(this.props.display[2], 'block') ? '#61666e':'#6c737c', 
          fontWeight: validate.equalsIgnoreCase(this.props.display[2], 'block') ? 'bold':'normal',
          textDecoration: validate.equalsIgnoreCase(this.props.display[2], 'block') ? 'underline':'none'}}>
          Service Dashboard
        </div>
      );
    }
  }

  render() {
    return (
      <div className="account-sidebar">
        <div className="account-sidebar-logo">Account Setting</div>
        <div className="account-sidebar-item-container">
          {this.renderAccountSetting()}
          {this.renderUserSetting()}
          {this.renderServiceSetting()}
        </div>
      </div>
    );
  }
}

export default Switch;