import React, { Component } from 'react';
import firebase from 'firebase';
import { Link } from 'react-router-dom';
import { FaArrowLeft } from 'react-icons/fa';
import * as validate from '../../libs/validate';
import './Account.css';

import Sidebar from './components/setting-bar/AccountSidebar.jsx';
import Setting from './components/account-setting/AccountSetting.jsx';
import Payment from './components/account-payment/AccountPayment.jsx';
import Printer from './components/account-service/AccountService.jsx';

class Account extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: null,
      historys: null,
      display: ['block', 'none', 'none']
    }
  }

  componentDidMount() {
    this.getUser();
  }

  getUser() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({user: user}, () => {
          this.getHistory()
        });
      }
    })
  }

  getHistory = () => {
    // eslint-disable-next-line
    var history = Array()
    firebase.database().ref().child('historys')
    .once('value').then((snap) => {
      Object.values(snap.val()).map(obj => {
        Object.values(obj).map(item => {
          if (validate.equals(item.customer_id, this.state.user.uid)) {
            history.push(item)
          }
          return (null)
        })
        return (null)
      })
    })
    .then(() => {
      this.setState({historys: history})
    })
  }

  // set ว่า user select หน้าไหนเพื่อแสดงหน้าที่ uaer select
  onSelectPage = (page) => {
    switch (page) {
      case 'profile': this.setState({display: ['block', 'none', 'none']}); break;
      case 'payment': this.setState({display: ['none', 'block', 'none']}); break;
      case 'service': this.setState({display: ['none', 'none', 'block']}); break;
      default: this.setState({display: ['none', 'none', 'none']}); break;
    }
  }

  renderAccountSetting() {
    return (
      <Setting status={this.state.display[0]}/>
    );
  }

  renderPayment() {
    if (this.state.historys && this.state.user) {
      return (
        <Payment 
          historys={this.state.historys}
          uid={this.state.user.uid}
          status={this.state.display[1]}
        />
      );
    }
  }

  renderPrinter() {
    return (
      <Printer status={this.state.display[2]}/>
    );
  }

  render() {
    localStorage.setItem('SideBar', 'hidden');
    return (
      <div className="account-container">
        <Sidebar setSelect={this.onSelectPage} display={this.state.display}/>
        <Link to="/" className="back-btn mt-2 ml-2"><FaArrowLeft/> Back</Link>
        <div className="account-detail-border">  
          {this.renderAccountSetting()}
          {this.renderPayment()}
          {this.renderPrinter()}
        </div>
      </div>
    );
  }
}

export default Account;