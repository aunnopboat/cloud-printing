import React, { Component } from 'react';
import firebase from 'firebase';
import swal from 'sweetalert';
import { Base64 } from 'js-base64';
import * as validate from '../../../libs/validate';
import { FaLock, FaLockOpen, FaKey, FaCircle } from 'react-icons/fa';
import './Login.css';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login_click: false,
      username: String(),
      password: String()
    }
  }

  handleChange = (event) => {
    this.setState({[event.target.name]: event.target.value})
  }

  onEnter = (event) => {
    if (validate.equalsIgnoreCase(event.key, 'Enter')) {
      this.checkUsername(this.state.username.toLocaleLowerCase());
    }
  }

  // user login เข้าระบบได้ email(สามารถใช้ username แทนได้) และ password
  signIn = (username) => {
    firebase.auth()
    .signInWithEmailAndPassword(
      username, 
      Base64.encode(this.state.password)
    )
    .then(() => {
      window.location.replace('/');
    })
    .catch((error) => {
      this.setState({
        username: String(),
        password: String(),
        login_click: false
      }, () => {
        swal({
          title: "Failed to login !!", 
          text: `${error}`, 
          icon: "error"
        }).then(() => {
          firebase.auth().signOut();
        });
      });
    });
  }

  // เช็คว่า user กรอก username หรือ email ก่อนที่จะทำการ login
  checkUsername = (email) => {
    this.setState({login_click: true}, () => {
      if (!validate.isEmail(email)) {
        var username = String();
        firebase.database().ref().child('logins').once('value')
        .then((snap) => {
          if (snap.val()) {
            Object.keys(snap.val()).map(key => {
              if (validate.equals(snap.val()[key].username, email)) {
                username = snap.val()[key].email
                this.signIn(username);
              }
              return (null)
            });
            if (!validate.notEmpty(username)) {
              swal({
                title: "Failed to login !!", 
                text: "Plaese check your username and password.", 
                icon: "error"
              }).then(() => {
                this.setState({
                  username: String(),
                  password: String(),
                  login_click: false
                });
              });
            }
          } else {
            swal({
              title: "Failed to login !!", 
              text: `Not found user in database`, 
              icon: "error"
            }).then(() => {
              window.location.reload();
            });
          }
        })
      } else {
        this.signIn(email);
      }
    })
  }

  render() {
    return(
      <div className="signin-container" onKeyPress={this.onEnter}>
        <div className="sign-area">
          <h4 className="mt-3 mb-5"><strong>Login</strong></h4>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text" id="username"><FaLock/> </span>
            </div>
            <input type="text" className="form-control" name="username" value={this.state.username}
              placeholder="Username" onChange={this.handleChange}/>
          </div>
          <div className="input-group mb-4">
            <div className="input-group-prepend">
              <span className="input-group-text" id="password"><FaKey/> </span>
            </div>
            <input type="password" className="form-control" name="password" value={this.state.password}
              placeholder="Password" onChange={this.handleChange}/>
          </div>
          <button type="button" className="btn btn-light mt-3" hidden={this.state.login_click}
            onClick={() => {this.checkUsername(this.state.username.toLocaleLowerCase())}}><FaLockOpen/> Login</button>
          <button type="button" className="btn btn-light mt-4" disabled hidden={!this.state.login_click}>
            <FaCircle className="wait-btn"/><FaCircle className="wait-btn"/><FaCircle/>
          </button>
        </div>
      </div>    
    );  
  }
}

export default Login;