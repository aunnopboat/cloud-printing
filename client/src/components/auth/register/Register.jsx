import React, { Component } from 'react';
import firebase from 'firebase';
import swal from 'sweetalert';
import { Base64 } from 'js-base64';
import * as validate from '../../../libs/validate';
import * as filter from '../../../libs/filter';
import { 
  FaUserEdit, 
  FaEnvelope, 
  FaLock, 
  FaKey, 
  FaUsersCog, 
  FaUserPlus,
  FaCircle } from 'react-icons/fa';
import './Register.css';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      regis_click: false,
      name: String(),
      email: String(),
      username: String(),
      password: String(),
      tel: String(),
      confirm: String(),
      type: String(),
      free_username: true,
    }
  }

  handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({[name] : event.target.value}, () => {
      if (validate.equals(name, 'username')) {
        this.freeUsername();
      } else if (validate.equals(name, 'tel')) {
        this.filterTel(value)
      }
    });
  }

  onEnter = (event) => {
    if (validate.equalsIgnoreCase(event.key, 'Enter')) {
      this.renderRegisBtn();
    }
  }

  filterTel = (value) => {
    this.setState({tel: filter.onlyNumber(value)})
  }

  // set status ของปุ่ม register
  renderRegisBtn = () => {
    if (
      validate.notEmpty(this.state.name) && 
      validate.isEmail(this.state.email) && 
      validate.notEmpty(this.state.username) && 
      validate.passIsvalid(this.state.password) && 
      validate.equals(this.state.confirm, this.state.password) && 
      validate.notEmpty(this.state.type) &&
      this.state.free_username) {
      return(
        <div>
          <button type="button" className="btn btn-light mt-4" onClick={this.registerUser} hidden={this.state.regis_click}>
            <FaUserPlus/> Register
          </button>
          <button type="button" className="btn btn-light mt-4" disabled hidden={!this.state.regis_click}>
            <FaCircle className="wait-btn"/><FaCircle className="wait-btn"/><FaCircle/>
          </button>
        </div>
      );
    } else {
      return(
        <button type="button" className="btn btn-light mt-4" disabled>
          <FaUserPlus/> Register
        </button>
      );
    }
  }

  // สร้าง account ของ user จาก email และ password
  registerUser = () => {
    this.setState({regis_click: true}, () => {
      firebase.auth().createUserWithEmailAndPassword(this.state.email.toLocaleLowerCase(), Base64.encode(this.state.password))
      .then(() => {
        const data = {
          uid: firebase.auth().currentUser.uid,
          name: this.state.name,
          tel: this.state.tel,
          email: this.state.email.toLocaleLowerCase(),
          username: this.state.username,
          password: Base64.encode(this.state.password),
          user_type: this.state.type,
          picture: (Math.floor(Math.random() * Math.floor(4)) + 1),
          ban_user: false
        }
        firebase.database().ref().child('users').child(firebase.auth().currentUser.uid).set(data);
        if (validate.equalsIgnoreCase(this.state.type, 'SERVICE')) {
          const service = {
            name: this.state.name,
            email: this.state.email.toLocaleLowerCase(),
            account_number: String(),
            QRcode_link: String(),
            bank_name: String(),
            address: String(),
            promptpay: String(),
            b_price: 0,
            c_price: 0,
            latitude: 0,
            longitude: 0,
          }
          firebase.database().ref().child('services').child(firebase.auth().currentUser.uid).set(service);
        }
        const user = {
          username: this.state.username.toLocaleLowerCase(),
          email: this.state.email.toLocaleLowerCase()
        }
        const upload = firebase.database().ref().child('logins').push(user)
        const id = upload.getKey();
        upload.update({
          id: id
        }).then(() => {
          swal({
            title: "Register success",
            icon: "success",
            timer: 2000
          }).then(() => {
            firebase.auth().signOut()
            .then(() => {
              window.location.replace('/login');
            })
          });
        });
      })
      .catch(error => {
        swal({
          title: "Failed to Register", 
          text: `${error}`, 
          icon: "error"
        }).then(() => {
          this.setState({regis_click: false});
        });
      })
    })
  }

  freeUsername = () => {
    var result = true;
    firebase.database().ref().child('logins')
    .once('value').then((snap) => {
      if (snap.val()) {
        Object.values(snap.val()).map(item => {
          if (validate.equalsIgnoreCase(item.username, this.state.username)) {
            result = false;
          }
          return (null)
        })
      }
    }).then(() => {
      // console.log(result);
      this.setState({free_username: result})
    })
  }

  render() {
    return(
      <div className="reg-container" onKeyPress={this.onEnter}>
        <div className="reg-area">
          <h3 className="mt-1 mb-5"><strong>Register</strong></h3>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text"><FaUserEdit/> </span>
            </div>
            <input type="text" className={"form-control" + (validate.notEmpty(this.state.name) ? ' is-valid' : String())} 
              name="name" placeholder="Full Name" value={this.state.name} onChange={this.handleChange}/>
          </div>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text"><FaEnvelope/> </span>
            </div>
            <input type="email" className={"form-control" + (validate.isEmail(this.state.email) ? ' is-valid' : String())} 
              name="email" placeholder="Email" value={this.state.email} onChange={this.handleChange}/>
          </div>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text"><FaLock/> </span>
            </div>
            <input type="text" className={"form-control" + (validate.telIsvalid(this.state.tel.toString()) ? ' is-valid' : String())} 
              name="tel" placeholder="Telephone" value={this.state.tel} onChange={this.handleChange}/>
          </div>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text"><FaLock/> </span>
            </div>
            <input type="text" className={"form-control" + (validate.notEmpty(this.state.username) && this.state.free_username ? ' is-valid' : String())} 
              name="username" placeholder="Username" value={this.state.username} onChange={this.handleChange}/>
          </div>
          <div className="input-group mb-3" data-toggle="tooltip" title="Must have 6 characters or more than">
            <div className="input-group-prepend">
              <span className="input-group-text"><FaKey/> </span>
            </div>
            <input type="password" className={"form-control" + (validate.passIsvalid(this.state.password) ? ' is-valid' : String())} 
              name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange}/>
          </div>
          <div className="input-group mb-3" data-toggle="tooltip" title="Must match with password">
            <div className="input-group-prepend">
              <span className="input-group-text"><FaKey/> </span>
            </div>
            <input type="password" className={"form-control" + (validate.equals(this.state.confirm, this.state.password) ? ' is-valid' : String())}
              name="confirm" placeholder="Confirm Password" value={this.state.confirm} onChange={this.handleChange}/>
          </div>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text"><FaUsersCog/> </span>
            </div>
            <select className={"form-control" + (validate.notEmpty(this.state.type) ? ' is-valid' : String())} 
              name="type" value={this.state.type} onChange={this.handleChange} 
              style={{color: (validate.notEmpty(this.state.type)? '#495057' : 'gray')}}>
                <option value="">[ Please select user type ]</option>
                <option value="USER">User</option>
                <option value="SERVICE">Service Person</option>
            </select>
          </div>
          {this.renderRegisBtn()}
        </div>
      </div>    
    );  
  }
}

export default Register;