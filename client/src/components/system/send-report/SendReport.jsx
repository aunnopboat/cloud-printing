import React, { Component } from 'react';
import firebase from 'firebase';
import swal from 'sweetalert';
import * as validate from '../../../libs/validate';
import './SendReport.css';

class SendReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: String(),
      text: String()
    }
  }

  handleChange = (event) => {
    this.setState({[event.target.name] : event.target.value})
  }

  sendReport = () => {
    if (validate.notEmpty(this.state.type) && validate.notEmpty(this.state.text)) {
      const data = {
        report_type: this.state.type,
        report_text: this.state.text,
        report_sender: firebase.auth().currentUser.uid
      }
      firebase.database().ref().child('reports')
      .push(data).then(() => {
        swal({
          title: "Send report success",
          icon: "success",
          timer: 2000
        })
        .then(() => {
          this.setState({
            type: String(),
            text: String()
          })
        })
      })
    } else {
      swal({
        title: `Please ${!validate.notEmpty(this.state.type) ? 'select report type' : 'input report text'}`,
        icon: "error",
        timer: 3000
      })
    }
  }

  render() {
    return (
      <div className="report-container">
        <div className="report-border">
          <h3 className="mb-3 mt-2"><strong>Report</strong></h3>
          <div className="mb-2">
          <select className="report-select form-control" name="type" value={this.state.type}
            style={{color: validate.notEmpty(this.state.type) ? '#000' : '#999'}}
            onChange={this.handleChange}>
            <option value="">- Select Type -</option>
            <option value="SYSTEM_PROBLEM">System Problems</option>
            <option value="USER-REPORT">User report</option>
          </select>
          </div>
          <div className="mb-1">
            <textarea className="report-text" placeholder="Report detail" 
              name="text" value={this.state.text} onChange={this.handleChange}/>
          </div>
          <button type="button" className="btn-send-report mb-1" onClick={this.sendReport}>Send Report</button>
        </div>
      </div>
    );
  }
}

export default SendReport;