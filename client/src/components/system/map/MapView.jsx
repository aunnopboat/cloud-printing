import React, { Component } from 'react';
import firebase from 'firebase';
import { Spinner } from 'reactstrap';
import { FaSearch, FaArrowLeft } from 'react-icons/fa';
import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";
import { MAP_TOKEN  } from '../../../resources/MapToken';
import * as validate from '../../../libs/validate';
import * as filter from '../../../libs/filter';
import swal from 'sweetalert';
import './MapView.css';

const Map = ReactMapboxGl({
  accessToken: MAP_TOKEN
});

class MapView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      latitude: 0,
      longitude: 0,
      pinSet: null,
      services: null,
      position: null,
      select_service: null,
      search_text: String()
    }
  }

  componentDidMount() {
    this.getLocation()
  }

  hendleSearch = (event) => {
    var value = event.target.value;
    if (validate.notEmpty(value)) {
      var getFilter = filter.filterName(value, this.state.services)
      this.setState({select_service: getFilter})
    } else {
      this.setState({select_service: null})
    }
    this.setState({search_text: value})
  }

  // รับ location จริง ของ user
  getLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(location => {
        this.setState({
          latitude: parseFloat(location.coords.longitude),
          longitude: parseFloat(location.coords.latitude),
          position: [(parseFloat(location.coords.longitude) + 0.0145), parseFloat(location.coords.latitude)]
        }, () => {
          this.getPin();
        })
      })
    } else {
      swal({
        title: "Geolocation is not supported by this browser.", 
        text: "Cannot get geolocation from this browser.", 
        icon: "warning",
        timer: 2000
      }).then(() => {
        this.setState({
          latitude: 0,
          longitude: 0,
          position: [0.0145, 0]
        }, () => {
          this.getPin();
        })
      })
    }
  }

  getPin = () => {
    // eslint-disable-next-line
    var setPin = Array();
    // eslint-disable-next-line
    var setService = Array();
    firebase.database().ref().child('services').once('value')
    .then((snap) => {
      if (snap.val()) {
        Object.keys(snap.val()).map(key => {
          setService.push(snap.val()[key]);
          setPin.push([snap.val()[key].latitude, snap.val()[key].longitude]);
          return (null);
        })
      }
    }).then(() => { 
      this.setState({
        pinSet: setPin,
        services: setService
      })
    })
  }

  goToPin = (event) => {
    var value = event.target.id.split('/');
    if (validate.equals(value[0], '0') && validate.equals(value[1], '0')) {
      this.setState({position: [(this.state.latitude + 0.0145), this.state.longitude]}, () => {
        swal({
          title: "Not found in map.",
          icon: "warning",
          timer: 2000
        })
      });
    } else {
      this.setState({position: [(parseFloat(value[0]) + 0.0145), parseFloat(value[1])]});
    }
  }

  render() {
    localStorage.setItem('SideBar', 'hidden');
    if (this.state.pinSet) {
      return (
        <div className="map-container">
          <div className="map-border">
            <Map
              // eslint-disable-next-line
              style="mapbox://styles/mapbox/dark-v9"
              center={this.state.position}
              zoom={[14]}
              containerStyle={{
                height: "100vh",
                width: "100vw"
              }}>
              <Layer
                type="symbol"
                id="marker" 
                layout={{ "icon-image": "marker-11" }}>
                <Feature coordinates={[this.state.latitude, this.state.longitude]}/>
                { 
                  Object.values(this.state.pinSet).map((data, key) => {
                    return(
                      validate.equalsNumber(0, data[0]) && validate.equalsNumber(0, data[1]) ?
                      (<Feature key={key} coordinates={[this.state.latitude, this.state.longitude]}/>) : 
                      (<Feature key={key} coordinates={data}/>)
                    );
                  })
                }
              </Layer>
            </Map>
          </div>
          <div className="map-search">
            <div className="search-map">
              <div className="search-input">
                <div className="input-group mb-3">
                  <input type="text" className="form-control" placeholder="Search" onChange={this.hendleSearch}/>
                  <div className="input-group-append">
                    <span className="input-group-text"><FaSearch/></span>
                  </div>
                </div>
              </div>
            </div>
            <div className="search-data-border">
              <div className="data-item-area">
                {
                  this.state.select_service ? 
                    Object.values(this.state.select_service).map((data, key) => {
                      return (
                        <div key={key} className="data-item" onClick={this.goToPin}>
                          <div className="data-item-paddding" id={`${data.latitude}/${data.longitude}`} >
                            {data.name}
                          </div>
                        </div>
                      );
                    }) : 
                  (null)
                }
              </div>
            </div>
            <div className="map-back mb-5">
              <div className="right-bar-back-btn" 
                onClick={() => {window.location.replace('/')}}>
                <FaArrowLeft/> Back
              </div>
            </div>
          </div>
        </div>  
      );
    } else {
      return (
        <Spinner className="mt-2" color="light" />
      )
    }  
  }
}

export default MapView;