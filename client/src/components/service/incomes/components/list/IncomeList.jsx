import React, { Component } from 'react';
import * as validate from '../../../../../libs/validate';
import { MONTH } from '../../../../../resources/DataSet';

import List from './components/IncomeItem.jsx';

class IncomeList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      histories: this.props.histories,
      year: this.props.year,
      setRender: null
    }
  }

  componentDidMount() {
    this.filterHistory()
  }

  componentWillReceiveProps(nextProps) {
    this.setState({year: parseInt(nextProps.year)}, () => {
      this.filterHistory()
    });
  }

  filterHistory = () => {
    const { histories, year } = this.state;
    const filter = [[],[],[],[],[],[],[],[],[],[],[],[]];
    this.setState({setRender: null}, () => {
      Object.values(histories).map(item => { 
        const itemMonth = (parseInt(new Date(item.time).toLocaleDateString("en-GB").toString().split('/')[1]) - 1)
        const itemYear = new Date(item.time).toLocaleDateString("en-GB").toString().split('/')[2];
        if (validate.equalsNumber(parseInt(itemYear), year)) {
          filter[itemMonth].push(item)
          this.setState({setRender: filter})
        }
        return (null)
      })
    })
  }

  render() {
    const { setRender, year } = this.state;
    return (
      <div className="income-list-border">
        {
          Object.values(MONTH).map((item, key) => {
            return(
              <List 
                key={key} 
                name={`${item} ${year}`} 
                // eslint-disable-next-line
                item={setRender ? setRender[key] : Array()}
              />
            )
          })
        }
      </div>
    );
  }
}

export default IncomeList;