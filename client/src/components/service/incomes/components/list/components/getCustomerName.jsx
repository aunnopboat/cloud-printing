import React, { Component } from 'react';
import firebase from 'firebase';
import { FaCircle } from 'react-icons/fa';

class IncomeItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: false
    }
  }

  componentDidMount() {
    this.getName()
  }

  getName() {
    firebase.database().ref('users').child(this.props.id)
    .once('value').then((snap) => {
      this.setState({name: snap.val().name})
    })
  }

  render() {
    return (
      <p className="table-value"><FaCircle style={{fontSize: '8px'}}/> {this.state.name}</p>
    );
  }
}

export default IncomeItem;