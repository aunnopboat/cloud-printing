import React, { Component } from 'react';
import { FaChevronDown, FaChevronUp } from 'react-icons/fa';
import { Collapse } from 'reactstrap';

import Name from './getCustomerName.jsx';

class IncomeItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openCollapse: false
    }
  }

  render() {
    const { openCollapse } = this.state
    return (
      <div className="income-item mt-2 ml-2 row">
        <div className={`income-title ${this.props.item.length > 0 ? 'title-up-size' : String()}`}
          onClick={this.props.item.length > 0 ? this.statusCollapse : null}>
          {this.props.name}
        </div>
        {
          this.props.item.length > 0 ?
          (
            <div className="item-arrow" onClick={this.props.item.length > 0 ? this.statusCollapse : null}>
              { openCollapse ? <FaChevronUp/> : <FaChevronDown/> }
            </div> 
          ): 
          (null)
        }
        {
          this.props.item.length > 0 ? 
          (
            <Collapse className="collapse-area income-collapse mt-2" isOpen={openCollapse}>
              {
                Object.values(this.props.item).map((item, key) => {
                  return (
                    <div key={key} className="income-table">
                      <div className="income-table-item row">
                        <div className="col-sm-3">
                          <strong>Date</strong>
                          <p className="table-value">{new Date(item.time).toLocaleDateString("en-GB").toString()}</p>
                        </div>
                        <div className="col-sm-4">
                          <strong>Customer Name</strong>
                          <Name id={item.customer_id}/>
                        </div>
                        <div className="col-sm-5">
                          <strong>Money</strong>
                          <p className="table-value">{item.income} THB</p>
                        </div>
                      </div>
                    </div>
                  )
                })
              }
            </Collapse>
          ) : 
          (null)
        }
      </div>
    );
  }

  statusCollapse = () => {
    this.setState({openCollapse: !this.state.openCollapse});
  }
}

export default IncomeItem;