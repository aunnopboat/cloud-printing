import React, { Component } from 'react';
import * as validate from '../../../../../libs/validate';
import { Line } from 'react-chartjs-2';
import { MONTH, CHART_COLOR } from '../../../../../resources/DataSet';

class IncomeGraph extends Component {
  constructor(props) {
    super(props);
    this.startChart = {
      labels: MONTH,
      datasets:[{
        label:'Service Incomes',
        data:[],
        backgroundColor: CHART_COLOR
      }]
    } 
    this.state = {
      histories: this.props.histories,
      year: this.props.year,
      setRender: null
    }
  }

  componentDidMount() {
    this.filterTotalIncome()
  }

  componentWillReceiveProps(nextProps) {
    this.setState({year: parseInt(nextProps.year)}, () => {
      this.filterTotalIncome()
    });
  }

  filterTotalIncome = () => {
    const { histories, year } = this.state;
    const filter = Array(12).fill(0);
    Object.values(histories).map((item) => { 
      const itemMonth = (parseInt(new Date(item.time).toLocaleDateString("en-GB").toString().split('/')[1]) - 1)
      const itemYear = new Date(item.time).toLocaleDateString("en-GB").toString().split('/')[2];
      if (validate.equalsNumber(parseInt(itemYear), year)) {
        filter[itemMonth] = filter[itemMonth] + item.income;
      }
      const data = this.startChart;
      data.datasets[0].data = filter;
      this.setState({setRender: data})
      return (null)
    })
  }

  render() {
    return (
      <div className="income-graph-border mt-4">
        <h4 className="mb-3">Incomes Graph</h4>
        <div className="graph-border d-flex justify-content-center">
          {
            this.state.setRender ? 
            (
              <Line
                height="100%"
                data={this.state.setRender}
                options={{
                  title:{
                    fontSize:25
                  },
                  legend:{
                    display: null
                  }
                }}
              />
            ) :
            (null)
          }
        </div>
      </div>
    );
  }
}

export default IncomeGraph;