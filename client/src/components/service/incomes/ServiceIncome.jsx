import React, { Component } from 'react';
import firebase from 'firebase';
import { Spinner } from 'reactstrap';
import * as validate from '../../../libs/validate';
import './ServiceIncome.css';

import List from './components/list/IncomeList.jsx';
import Graph from './components/graph/IncomeGraph.jsx';

class ServiceIncome extends Component {
  constructor(props) {
    super(props);
    this.tabSet = ['income-list', 'income-graph']
    this.state = {
      year: new Date().getFullYear(),
      setYear: null,
      user: null,
      histories: null,
      tabSelect: this.tabSet[0]
    }
  }

  componentDidMount() {
    this.getData()
    this.getSetYear()
  }

  handleChange = (event) => {
    this.setState({[event.target.name] : event.target.value});
  }

  getData = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        firebase.database().ref().child('users').child(user.uid)
        .once('value').then((snap) => {
          this.setState({user: snap.val()})
        })
        .then(() => {
          this.getIncomes()
        })
      }
    })
  }

  getIncomes = () => {
    // eslint-disable-next-line
    var history = Array()
    firebase.database().ref().child('historys').child(this.state.user.uid)
    .once('value').then((snap) => {
      if (snap.val()) {
        Object.keys(snap.val()).map(key => {
          const item = Object.assign({}, snap.val()[key], {id: key})
          history.push(item)
          return (null)
        })
      }
    })
    .then(() => {
      this.setState({histories: history})
    })
  }

  getSetYear = () => {
    // eslint-disable-next-line
    var years = Array()
    for (let i = (this.state.year - 50); i <= this.state.year; i++) {
      years.push(i)
    }
    this.setState({setYear: years});
  }

  render() {
    return (
      <div className="income-container">
        <div className="income-border">
          <h4 className="income-logo mb-2">Service Incomes</h4>
          <div className="income-type-area mb-2 row">
            <select className="income-type ml-3" name="year" value={this.state.year} onChange={this.handleChange}>
              {
                this.state.setYear ?
                (
                  Object.values(this.state.setYear).map((value, key) => {
                    return (<option key={key} value={value}>{value}</option>)
                  })
                ) : 
                (null)
              }
            </select>
          </div>
          <div className="income-present-area">
            <div className="income-tabs">
              {
                this.tabSet.map((item, key) => {
                  return (
                    <button name={item} onClick={() => this.setState({tabSelect: item})} key={key}
                      className={`income-tab ${validate.equals(item, this.state.tabSelect) ? 'active' : String()}`}>
                      {validate.equals('income-list', item) ? 'Incomes List' : 'Graph'}
                    </button>
                  );
                })
              }
            </div>
            <div className={`income-present-border ${validate.equals('income-list', this.state.tabSelect) ? 'income-present-list' : 'income-present-graph'}`}>
              {
                this.state.year && this.state.histories ?
                (
                  validate.equals('income-list', this.state.tabSelect) && (this.state.histories.length > 0) ? 
                  (<List histories={this.state.histories} year={this.state.year} />) : 
                  (<Graph histories={this.state.histories} year={this.state.year} />)
                ) : 
                (<Spinner className="mt-2" color="light" />)
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceIncome;