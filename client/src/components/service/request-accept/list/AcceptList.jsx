import React, { Component } from 'react';
import firebase from 'firebase';
import { Spinner } from 'reactstrap';
import * as validate from '../../../../libs/validate';
import './AcceptList.css';

import List from '../item/AcceptItem.jsx';

class AcceptList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      requests: null,
      type: localStorage.getItem('type')
    }
  }

  componentDidMount() {
    this.getRequest(this.state.type);
  }

  handleChange = (event) => {
    localStorage.setItem('type', event.target.value);
    this.setState({[event.target.name] : event.target.value}, () => {
      this.getRequest(this.state.type);
    });
  }

  getRequest(type) {
    this.setState({requests: null}, () => {
      // eslint-disable-next-line
      var request = Array();
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          firebase.database().ref().child('requests')
          .once('value').then((snap) => {
            if (snap.val()) {
              Object.keys(snap.val()).map(key => {
                if (validate.equalsIgnoreCase(snap.val()[key].status, type) && 
                  validate.equalsIgnoreCase(snap.val()[key].service_id, user.uid)) {
                  var req = Object.assign({}, snap.val()[key], {id: key})
                  request.push(req)
                }
                return (null)
              });
            }
          })
          .then(() => {
            this.setState({requests: request})
          })
        }
      })
    })
  }

  render() {
    return (
      <div className="request-container">
        <div className="request-border">
          <h4 className="request-logo">Accept Request List</h4>
          <div className="request-type-area row">
            <select className="request-type ml-3" name="type" value={this.state.type} onChange={this.handleChange}>
              <option value="WAIT_PAYMENT">WAIT PAYMENT</option>
              <option value="IN_PROGRESS">IN PROGRESS</option>
              <option value="COMPLETE">COMPLETE</option>
            </select>
          </div>
          <div className="request-list mt-2">
            {
              this.state.requests ? 
              ( 
                this.state.requests.length > 0 ?
                (
                  Object.values(this.state.requests).map((request, key) => {
                    return (<List key={key} request={request}/>);
                  })
                ) : 
                (null)
              ) : 
              <Spinner className="mt-2" color="light" />
            }
          </div>
        </div>
      </div>
    );
  }
}

export default AcceptList;