import React, { Component } from 'react';
import firebase from 'firebase';
import { Collapse } from 'reactstrap';
import Modal from 'react-bootstrap/Modal';
import swal from 'sweetalert';
import * as validate from '../../../../libs/validate';
import { FaChevronDown, FaChevronUp } from 'react-icons/fa';
import './AcceptItem.css';

class AcceptItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      openCollapse: false,
      openModal: false,
      customer: null,
      request: this.props.request,
      type: this.props.request.status,
      prices: this.props.request.prices,
      imgPath: null,
      rejectDescription: String(),
      customerModal: false,
    }
  }

  componentDidMount() {
    this.getCustomer()
  }

  getCustomer() {
    firebase.database().ref().child('users').child(this.state.request.user_id)
    .once('value').then((snap) => {
      this.setState({customer: snap.val()});
    })
    if (validate.notEmpty(this.state.request.receipt)) {
      this.setState({imgPath: this.state.request.receipt})
    }
  }

  hendleChange = (event) => {
    this.setState({[event.target.name] : event.target.value})
  }

  alertChange = (event) => {
    var value = event.target.value;
    if (!validate.equalsIgnoreCase(value, this.state.type)) {
      if (!validate.equalsIgnoreCase(value, 'REJECT')) {
        swal({
          title: "Are you sure?",
          text: `Change state from ${this.state.type} to ${value}`,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((res) => {
          if (res) {
            this.changeType(value)
          }
        });
      } else {
        swal({
          title: "Are you sure?",
          text: `Change state from ${this.state.type} to ${value}`,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((res) => {
          if (res) {
            this.rejectRequest()
          }
        });
      }
    }
  }

  rejectRequest = () => {
    firebase.database().ref().child('requests').child(this.state.request.id)
    .update({
      status: "PENDING",
      prices: String(),
      service_id: String(),
      receipt: String()
    })
    .then(() => {
      swal({
        title: "Reject request success",
        icon: "success",
        timer: 2000
      }).then(() => {
        window.location.reload();
      });
    })
  }

  changeType = (value) => {
    firebase.database().ref().child('requests').child(this.state.request.id)
    .update({
      status: value
    }).then(() => {
      window.location.reload();
    })
  }

  rejectPayment = () => {
    swal({
      title: "Are you sure?",
      text: "Reject payment receipt.",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((res) => {
      if (res) {
        if (validate.notEmpty(this.state.rejectDescription)) {
          this.removeReceipt(this.state.rejectDescription)
        } else {
          swal({
            title: "Type something before reject.",
            icon: "error",
            timer: 2000
          });
        }
      }
    });
  }

  removeReceipt = (description) => {
    const { request } = this.props;
    firebase.database().ref().child('requests').child(request.id)
    .update({
      receipt: String(),
      receipt_name: String(),
      reject_description: description,
      payment_status: 'REJECT'
    })
    .then(() => {
      firebase.storage().ref().child('receipts').child(request.id).child(request.receipt_name)
      .delete().then(() => {
        firebase.database().ref().child('historys').child(request.service_id).child(request.id)
        .remove().then(() => {
          swal({
            title: "Reject payment success",
            icon: "success",
            timer: 2000,
          })
          .then(() => {
            window.location.reload();
          })
        })
      })
    })
  }

  render() {
    const { openCollapse, openModal, request, customer, customerModal } = this.state;   
    return (
      <div className="request-item mt-2 ml-2 row">
        <div className="item-title" onClick={this.statusCollapse}>
          {request.file_name}
        </div>
        <div className="item-status">
          <select className="item-select" name="type" value={this.state.type} onChange={this.alertChange}>
            <option value="WAIT_PAYMENT">WAIT PAYMENT</option>
            <option value="IN_PROGRESS">IN PROGRESS</option>
            <option value="COMPLETE">COMPLETE</option>
            {
              !validate.equalsInSet(request.payment_status, ['PAID', 'CASH']) ?
              (<option value="REJECT">REJECT</option>) :
              (null)
            }
          </select>
        </div>
        <div className="item-arrow" onClick={this.statusCollapse}>
          { openCollapse ? <FaChevronUp/> : <FaChevronDown/> }
        </div>
        <Collapse className="collapse-area mt-2" isOpen={openCollapse}>
          <div className="collapse-item row mt-2">
            <div className="collapse-item-head ml-1"><strong>File owner</strong></div>
            <div className="collapse-item-item">: 
              {
                customer ? 
                  // eslint-disable-next-line
                  <a herf="*" className="ml-1" onClick={this.customerModal}><u className="customer-info">{customer.name}</u></a> : 
                  String()
              }
            </div>
          </div>
          <div className="collapse-item row">
            <div className="collapse-item-head ml-1"><strong>File path</strong></div>
            <div className="collapse-item-item">:
              <a href={request.file_link} className="path-tag" target="_blank" rel="noopener noreferrer"><u className="ml-1">Link file</u></a>
            </div>
          </div>
          <div className="collapse-item row">
            <div className="collapse-item-head ml-1"><strong>Copy</strong></div>
            <div className="collapse-item-item">: {request.quantity}</div>
          </div>
          <div className="collapse-item row">
            <div className="collapse-item-head ml-1"><strong>Color Request</strong></div>
            <div className="collapse-item-item">: {request.type}</div>
          </div>
          <div className="collapse-item row">
            <div className="collapse-item-head ml-1"><strong>Prices</strong></div>
            <div className="collapse-item-item">: {request.prices} THB</div>
          </div>
          <div className="collapse-item row mb-2">
            <div className="collapse-item-head ml-1"><strong>Payment Status</strong></div>
            <div className="collapse-item-item">: 
              <strong className={`${validate.equalsIgnoreCase(request.payment_status, 'PAID') ? 'paid-review ' : String()} ml-1`} 
                onClick={validate.equalsIgnoreCase(request.payment_status, 'PAID') ? this.openModal : null}
                style={{color: validate.equalsInSet(request.payment_status, ['PAID', 'CASH']) ? 'green' : 
                  validate.equalsIgnoreCase(request.payment_status, 'REJECT') ? 'red' : 'gray'}}
                >
                {request.payment_status}
              </strong>
            </div>
          </div>
        </Collapse>
        <Modal show={openModal} onHide={this.closeModal}>
          <div className="modal-bg">
            <Modal.Header>
              <Modal.Title style={{color: 'white'}}><strong>Payment</strong></Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-bg">
              {
                this.state.imgPath ? 
                (
                  <div className="img-border">
                    <img src={this.state.imgPath} alt="receipt" width="100%"/>
                  </div>
                ) :
                (null)
              }
              <div className="reject-item ml-1 mt-2 row">
                <div className="reject-item-head ml-1"><strong>Reject description</strong><br/>(For reject payment)</div>
                <div className="reject-item-item ml-1 mt-2">: 
                  <input type="text" name="rejectDescription" className="reject-input ml-1"
                    value={this.state.rejectDescription} onChange={this.hendleChange}/>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <button type="button" className="btn btn-secondary" onClick={this.closeModal}><strong>Close</strong></button>
              <button type="button" className="btn btn-danger" onClick={this.rejectPayment}><strong>Reject</strong></button>
            </Modal.Footer>
          </div>
        </Modal>
        <Modal show={customerModal} onHide={this.customerModal}>
          <div className="modal-bg">
            <Modal.Header>
              <Modal.Title style={{color: 'white'}}><strong>Customer Information</strong></Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-bg">
              <div className="payment-info-item mb-2 row">
                <div className="payment-info-head">Name</div>
                <div className="payment-info-body">: 
                  {
                    customer ? 
                     (
                       validate.notEmpty(customer.name) ? 
                       ` ${customer.name}` : 
                       ' -'
                    ) : 
                    String()
                  }
                </div>
              </div>
              <div className="payment-info-item mb-2 row">
                <div className="payment-info-head">Username</div>
                <div className="payment-info-body">: 
                  {
                    customer ? 
                     (
                       validate.notEmpty(customer.username) ? 
                       ` ${customer.username}` : 
                       ' -'
                    ) : 
                    String()
                  }
                </div>
              </div>
              <div className="payment-info-item mb-2 row">
                <div className="payment-info-head">Email</div>
                <div className="payment-info-body">: 
                  {
                    customer ? 
                     (
                       validate.notEmpty(customer.email) ? 
                       ` ${customer.email}` : 
                       ' -'
                    ) : 
                    String()
                  }
                </div>
              </div>
              <div className="payment-info-item row">
                <div className="payment-info-head">Telephone</div>
                <div className="payment-info-body">: 
                  {
                    customer ? 
                     (
                       validate.notEmpty(customer.tel) ? 
                       ` ${customer.tel}` : 
                       ' -'
                    ) : 
                    String()
                  }
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer/>
          </div>
        </Modal>
      </div>
    );
  }

  statusCollapse = () => {
    this.setState({openCollapse: !this.state.openCollapse});
  }

  customerModal = () => {
    this.setState({customerModal: !this.state.customerModal})
  }

  closeModal = () => {
    this.setState({openModal: false})
  }

  openModal = () => {
    this.setState({openModal: true})
  }
}

export default AcceptItem;