import React, { Component } from 'react';
import firebase from 'firebase';
import swal from 'sweetalert';
import { Spinner } from 'reactstrap';
import { FaUndo } from 'react-icons/fa';
import * as validate from '../../../../libs/validate';
import './UnAcceptList.css';

import List from '../../request-pending/item/UnAcceptItem.jsx';

class UnAcceptList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: null,
      service: null,
      requests: null
    }
  }

  componentDidMount() {
    this.getUser()
  }

  getUser = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        firebase.database().ref().child('users').child(user.uid)
        .once('value').then((snap) => {
          if (snap.val()) {
            this.setState({user: snap.val()})
          }
        })
        firebase.database().ref().child('services').child(user.uid)
        .once('value').then((snap) => {
          this.setState({service: snap.val()})
        })
        .then(() => {
          const { service } = this.state;
          const not_empty = (
            validate.notEmpty(service.QRcode_link) ||
            validate.notEmpty(service.promptpay) ||
            validate.notEmpty(service.account_number) ||
            validate.notEmpty(service.bank_name)
          );
          const not_zero = (
            !validate.equalsNumber(service.b_price, 0) &&
            !validate.equalsNumber(service.c_price, 0)
          )
          if (not_empty && not_zero) {
            this.getRequestPending()
          } else {
            swal({
              title: "Input your service payment information.",
              icon: "warning",
              timer: 3000
            })
            .then(() => {
              window.location.replace('/cloud/user_account')
            })
          }
        })
      }
    })
  }

  checkNear (lat, long) {
    const { service } = this.state;
    var latDiff, longDiff;
    var result = false;
    latDiff = Math.abs((Math.abs(parseFloat(lat)) - Math.abs(parseFloat(service.latitude))));
    longDiff = Math.abs((Math.abs(parseFloat(long)) - Math.abs(parseFloat(service.longitude))));
    if (latDiff < 0.05 && longDiff < 0.05) {
      result = true
    };
    return result;
  }

  getRequestPending = () => {
    this.setState({requests: null}, () => {
      // eslint-disable-next-line
      var request = Array();
      firebase.database().ref().child('requests')
      .once('value').then((snap) => {
        if (snap.val()) {
          Object.keys(snap.val()).map(key => {
            if (
              validate.equalsIgnoreCase(snap.val()[key].status, 'PENDING') &&
              this.checkNear(snap.val()[key].latitude, snap.val()[key].longitude)
            ) {
              var req = Object.assign({}, snap.val()[key], {id: key})
              request.push(req)
            }
            return (null);
          });
        }
      })
      .then(() => {
        this.setState({requests: request})
      })
    }) 
  }

  render() {
    return (
      <div className="unaccept-container">
        <div className="unaccept-border">
          <h4 className="unaccept-logo">Pending Request List</h4>
          <button type="button" className="unaccept-refresh mt-2" 
            onClick={() => this.getRequestPending()}>
            <FaUndo/> Refresh
          </button>
          <div className="unaccept-item-area mt-4">
            {
              this.state.requests ? 
              (
                this.state.requests.length > 0 ? 
                (
                  Object.values(this.state.requests).map((item, key) => {
                    return (
                      <List
                        key={key}
                        request={item}
                        service={this.state.service}
                      />
                    )
                  })
                ) :
                (null)
              ) :
              <Spinner className="mt-2" color="light" />
            }
          </div>
        </div>
      </div>
    );
  }
}

export default UnAcceptList;