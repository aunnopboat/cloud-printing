import React, { Component } from 'react';
import firebase from 'firebase';
import swal from 'sweetalert';
import * as validate from '../../../../libs/validate';
import * as calculate from '../../../../libs/calculate';
import { FaChevronDown, FaChevronUp } from 'react-icons/fa';
import { Collapse } from 'reactstrap';
import './UnAcceptItem.css';

class UnAcceptItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      request: this.props.request,
      service: this.props.service,
      openCollapse: false,
      customer: null
    }
  }

  componentDidMount() {
    this.getUser()
  }

  getUser = () => {
    const { request } = this.state;
    firebase.database().ref().child('users').child(request.user_id)
    .once('value').then((snap) => {
      this.setState({customer: snap.val()})
    })
  }

  acceptRequest = () => {
    const { service, request } = this.state;
    var costs = (calculate.costs(
      request.type === 'MONO' ? 
        service.b_price : service.c_price, 
      request.total_page, 
      request.quantity
    ));
    if (
      validate.equals(request.budget.toString(), '0') ||
      request.budget >= costs
    ) {
      firebase.database().ref().child('requests')
      .child(request.id).update({
        prices: costs,
        status: "WAIT_PAYMENT",
        service_id: firebase.auth().currentUser.uid
      })
      .then(() => {
        swal({
          title: "Get request success",
          icon: "success",
          timer: 2000
        }).then(() => {
          window.location.replace('/service/accept/list');
        });
      })
    } else {
      swal({
        title: "Cannot get request",
        text: "Customer max budget, not enough.",
        icon: "warning",
        timer: 2000
      })
    }
  }

  render() {
    const { openCollapse, request } = this.state;
    return (
      <div className="unaccept-item mt-2 ml-2 row" onClick={this.statusCollapse}>
        <div className="item-title">
          {request.file_name}
        </div>
        <div className="item-status">
          <div style={{color: 'gray'}}>
            <strong>{request.status}</strong>
          </div>
        </div>  
        <div className="item-arrow">
          { openCollapse ? <FaChevronUp/> : <FaChevronDown/> }
        </div>
        <Collapse className="collapse-area mt-2" isOpen={openCollapse}>
          <div className="collapse-item row mt-2">
            <div className="collapse-item-head ml-1"><strong>File owner</strong></div>
            <div className="collapse-item-item">: {this.state.customer ? this.state.customer.name : String()}</div>
          </div>
          <div className="collapse-item row">
            <div className="collapse-item-head ml-1"><strong>File path</strong></div>
            <div className="collapse-item-item">:
              <a href={request.file_link} target="_blank" rel="noopener noreferrer"> Link file</a>
            </div>
          </div>
          <div className="collapse-item row">
            <div className="collapse-item-head ml-1"><strong>Copy</strong></div>
            <div className="collapse-item-item">: {request.quantity}</div>
          </div>
          <div className="collapse-item row">
            <div className="collapse-item-head ml-1"><strong>Color Request</strong></div>
            <div className="collapse-item-item">: {request.type}</div>
          </div>
          <div className="collapse-item mb-2 row">
            <div className="collapse-item-head ml-1"><strong>Max Budget</strong></div>
            <div className="collapse-item-item">: {request.budget <= 0 ? 'None' : request.budget} THB</div>
          </div>
        </Collapse>
        {
          openCollapse ? 
          (
            <div className=" row" style={{marginBottom: '-15px'}}>
              <button type="button" className="unaccept-refresh ml-3 mt-2 mb-2" onClick={this.acceptRequest}>Accept</button>
            </div>
          ) : 
          (null)
        }
      </div>
    );
  }

  statusCollapse = () => {
    this.setState({openCollapse: !this.state.openCollapse});
  }
}

export default UnAcceptItem;