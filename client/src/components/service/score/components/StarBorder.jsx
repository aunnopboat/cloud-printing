import React, { Component } from 'react';
import StarRatings from 'react-star-ratings';
import * as calculate from '../../../../libs/calculate';

class StarBorder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fast: this.props.fast,
      cost: this.props.cost,
      quality: this.props.quality
    }
  }

  componentWillReceiveProps = (nextProps) => {
    this.setState({
      fast: nextProps.fast,
      cost: nextProps.cost,
      quality: nextProps.quality
    });
  }

  render() {
    return (
      <div className="score-star-border mt-3">
        <div className="score-star row">
          <div className="star-logo mt-2 ml-3 mr-5">Service's Fast</div>
            <StarRatings
              className="star-score"
              rating={this.state.fast}
              starRatedColor="rgb(255, 180, 0)"
              numberOfStars={5}
              starDimension="40px"
            />
            <div className="star-percent mt-2 ml-5">{ calculate.star(this.state.fast, 5) } %</div>
            <div className="star-emoji">{`${calculate.emoji(this.state.fast).emoji}  ${calculate.emoji(this.state.fast).value}`}</div>
        </div>
        <div className="score-star row">
          <div className="star-logo mt-2 ml-3 mr-5">Services Cost</div>
          <StarRatings
            className="star-score"
            rating={this.state.cost}
            starRatedColor="rgb(255, 180, 0)"
            numberOfStars={5}
            starDimension="40px"
          />
          <div className="star-percent mt-2 ml-5">{ calculate.star(this.state.cost, 5) } %</div>
          <div className="star-emoji">{`${calculate.emoji(this.state.cost).emoji}  ${calculate.emoji(this.state.cost).value}`}</div>
        </div>
        <div className="score-star row">
          <div className="star-logo mt-2 ml-3 mr-5">Service's Quality</div>
          <StarRatings
            className="star-score"
            rating={this.state.quality}
            starRatedColor="rgb(255, 180, 0)"
            numberOfStars={5}
            starDimension="40px"
          />
          <div className="star-percent mt-2 ml-5">{ calculate.star(this.state.quality, 5) } %</div>
          <div className="star-emoji">{`${calculate.emoji(this.state.quality).emoji}  ${calculate.emoji(this.state.quality).value}`}</div>
        </div>
      </div>
    );
  }
}

export default StarBorder;