import React, { Component } from 'react';
import firebase from 'firebase';
import { AVATAR } from '../../../../resources/images/user-avatar/avatar-item';

class Comment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: this.props.comments,
      customerModal: false,
      picture: null,
      name: null,
      username: null,
      email: null
    }
  }

  componentDidMount() {
    firebase.database().ref().child('users').child(this.state.comment.uid)
    .once('value').then((snap) => {
      this.setState({
        name: snap.val().name,
        picture: snap.val().picture,
        email: snap.val().email,
        username: snap.val().username
      })
    })
  }

  selectItem = () => {
    const { email, name, username } = this.state;
    if (name && username && email) {
      const customer = {
        name: name,
        email: email,
        username: username
      }
      this.props.selectCustomer(customer)
    }
  }

  render() {
    const { comment, picture, name } = this.state;
    return (
      <div className="comment-area mb-2"> 
        <div className="comment-user row">
          <img className="image-avater ml-2 mr-2" alt="user avatar" src={ picture ? AVATAR[picture] : AVATAR[0] } height="40"/>
          <div className="comment-detail" onClick={this.selectItem}>
            <div className="comment-user">{name ? name: String()}</div>
            <div className="comment-time">{new Date(comment.time).toLocaleDateString("en-GB").toString()}</div>
          </div>
        </div>
        <div className="comment-text mt-3 ml-4">
          {comment.comment}
        </div>
        <div className="comment-close mt-3"/>
      </div>
    );
  }
}

export default Comment;