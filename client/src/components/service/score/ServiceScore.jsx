import React, { Component } from 'react';
import firebase from 'firebase';
import * as validate from '../../../libs/validate';
import Modal from 'react-bootstrap/Modal';
import { Spinner } from 'reactstrap';
import './ServiceScore.css';

import Star from './components/StarBorder.jsx';
import Comment from './components/Comment.jsx';

class ServiceScore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      service_fast: -1,
      service_cost: -1,
      service_quality: -1,
      comments: null,
      customerModal: false,
      selectCustomer: null
    }
  }

  componentDidMount() {
    this.getData()
  }

  getData = () => {
    // eslint-disable-next-line
    const fast = Array(), cost = Array(), quality = Array(), comment = Array();
    var fast_score = 0, cost_score = 0, quality_score = 0;
    firebase.database().ref().child('scores')
    .once('value').then((snap) => {
      Object.values(snap.val()).map(item => {
        if (validate.equals(firebase.auth().currentUser.uid, item.service_id)) {
          fast.push(item.service_fast);
          cost.push(item.service_cost);
          quality.push(item.service_quality);
          var ment = {
            uid: item.customer_id,
            time: item.time,
            comment: item.comment
          }
          comment.push(ment)
        }
        return (null);
      })
    })
    .then(() => {
      if (fast.length > 0 && cost.length > 0 && quality.length > 0) {
        fast.forEach(score => {
          fast_score += score
        });
        cost.forEach(score => {
          cost_score += score
        });
        quality.forEach(score => {
          quality_score += score
        }); 
      }
    })
    .then(() => {
      if (fast.length > 0 && cost.length > 0 && quality.length > 0) {
        fast_score = (fast_score / fast.length);
        cost_score = (cost_score / cost.length);
        quality_score = (quality_score / quality.length);
      }
      this.setState({
        service_fast: fast_score,
        service_cost: cost_score,
        service_quality: quality_score,
        comments: comment
      })
    })
  }

  selectCustomer = (customer) => {
    this.setState({
      customerModal: true,
      selectCustomer: customer
    })
  }

  render() {
    const { customerModal, selectCustomer } = this.state;
    return (
      <div className="score-container">
        <div className="score-border">
          <h4 className="score-logo"><strong>Service Score</strong></h4>
          <Star 
            fast={this.state.service_fast}
            cost={this.state.service_cost}
            quality={this.state.service_quality}
          />
          <h5 className="mt-4">Comments</h5>
          <div className="score-comment-border mt-4">
            {
              this.state.comments?
              (
                this.state.comments.length > 0 ? 
                (
                  Object.values(this.state.comments).map((item, key) => {
                    return (
                      <Comment key={key} comments={item} selectCustomer={this.selectCustomer}/>
                    )
                  })
                ) :
                (null)
              ) :
              <Spinner className="mt-2" color="light" />
            }
          </div>
        </div>
        <Modal show={customerModal} onHide={this.closeModal}>
          <div className="modal-bg">
            <Modal.Header>
              <Modal.Title style={{color: 'white'}}><strong>Customer Information</strong></Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-bg">
              <div className="payment-info-item mb-2 row">
                <div className="payment-info-head">Name</div>
                <div className="payment-info-body">: 
                  {
                    selectCustomer ? 
                     (
                       validate.notEmpty(selectCustomer.name) ? 
                       ` ${selectCustomer.name}` : 
                       ' -'
                    ) : 
                    String()
                  }
                </div>
              </div>
              <div className="payment-info-item mb-2 row">
                <div className="payment-info-head">Username</div>
                <div className="payment-info-body">: 
                  {
                    selectCustomer ? 
                     (
                       validate.notEmpty(selectCustomer.username) ? 
                       ` ${selectCustomer.username}` : 
                       ' -'
                    ) : 
                    String()
                  }
                </div>
              </div>
              <div className="payment-info-item row">
                <div className="payment-info-head">Email</div>
                <div className="payment-info-body">: 
                  {
                    selectCustomer ? 
                     (
                       validate.notEmpty(selectCustomer.email) ? 
                       ` ${selectCustomer.email}` : 
                       ' -'
                    ) : 
                    String()
                  }
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer/>
          </div>
        </Modal>
      </div>
    );
  }

  closeModal = () => {
    this.setState({customerModal: false})
  }
}

export default ServiceScore;