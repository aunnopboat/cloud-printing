import React, { Component } from 'react';
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa';
import './Preview.css';

class Preview extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  renderFileDetail = () => {
    return (
      <div className="file-detail-border">
        <div className="detail-object">
          <strong>File name</strong>
          <p className="tab-value"> • { this.props.name }</p>
        </div>
        <div className="detail-object">
          <strong>File type</strong>
          <p className="tab-value"> • { this.props.type }</p>
        </div>
        <div className="detail-object">
          <strong>File owner</strong>
          <p className="tab-value"> • { this.props.owner }</p>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="preview-container">
        <div align="left" id="preview-field">
          <embed id="embed" title={`${this.props.name}.${this.props.type}`} src={this.props.link} 
            scrolling="yes"/> 
        </div>
        <div className="file-detail d-flex justify-content-left">
          {this.renderFileDetail()}
        </div>
        <div className="preview-back-btn mb-5">
          <div className="row">
            <div className="right-bar-back-btn ml-3" onClick={() => window.location.replace('/cloud/upload')}>
              <FaArrowLeft/> Cancel
            </div>
            <div className="right-bar-select-btn" onClick={this.props.openModal}>
              Send Request <FaArrowRight/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Preview;