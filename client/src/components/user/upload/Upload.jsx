import React, { Component } from 'react';
import firebase from 'firebase';
import swal from 'sweetalert';
import axios from 'axios';
import { FaUpload } from 'react-icons/fa';
import Modal from 'react-bootstrap/Modal';
import { path } from '../../../resources/DataSet';
import * as validate from '../../../libs/validate';
import './Upload.css';

import Preview from '../preview/Preview.jsx';

class Upload extends Component {
  constructor(props) {
    super(props);
    this.file = null;
    this.state = {
      open: false,
      isFileIn: false,
      isUpload: false,
      untilUpload: false,
      link: null,
      total_page: 0,
      up_time: null,
      file_id: null,
      file_type: null,
      owner: null,
      quantity: 1,
      budget: 0,
      type: 'MONO',
    }
  }

  componentDidMount() {
    this.getUser();
  }

  getUser() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        firebase.database().ref().child('users').child(user.uid)
        .once('value').then((snap) => {
          this.setState({owner: snap.val().name});
        })
        .then(() => {
          navigator.geolocation.getCurrentPosition(location => {
            this.setState({
              latitude: parseFloat(location.coords.longitude),
              longitude: parseFloat(location.coords.latitude)
            })
          }) 
        })
      }
    })
  }

  handleChange = (event) => {
    this.setState({[event.target.name] : event.target.value});
  }

  handleNumber = (event) => {
    var value = parseInt(event.target.value);
    if (validate.equalsIgnoreCase(event.target.name, 'quantity')) {
      if (value < 1 || isNaN(value)) {
        value = 1;
      }
    } else {
      if (value < 1 || isNaN(value)) {
        value = 0;
      }
    }
    this.setState({[event.target.name]: value});
  }

  // รับ file data จาก upload field
  onFileSelected = (event) => {
    if (firebase.auth().currentUser !== null) {
      this.file = event.target.files[0];
      this.setState({isFileIn: true});
    } else {
      swal({
        title: "Please login", 
        text: "Go to login page.", 
        icon: "warning",
        timer: 2000
      }).then(() => {
        window.location.replace('/login');
      });
    }
  }

  // upload file ลงใน firebase storage และรับ URL link หลัง upload เสร็จ
  uploadFile = () => {
    var date = new Date();
    if (this.file !== null) {
      this.setState({up_time: date.getTime().toString()}, () => {
        var uploadTask = firebase.storage().ref().child('files').child(this.state.up_time).child(this.file.name).put(this.file);
        uploadTask.on('state_changed', (snapshot) => {
          var progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
          this.setUntilUpload(true);
          var elem = document.getElementById("my-bar");
          elem.style.width = progress + '%';
        }, (error) => {
          swal({
            title: "Failed to upload", 
            text: `${error}`, 
            icon: "error"
          })
        }, () => {
          uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
            this.setUntilUpload(false);
            this.setUploadStatus(true);
            this.uploadDetail(this.state.up_time, downloadURL);
          });
        });
      });
    }
  }

  // upload detail ลงใน firebase database
  uploadDetail = (up_time, link) => {
    axios.post(`${path}/pdf-title`, {
      'link': link
    }).then((res) => {
      if (res.data.type !== undefined) {
        this.setState({
          total_page: res.data.totalPage, 
          file_type: res.data.type
        }, () => {
          const data = {
            file_name: this.file.name,
            file_link: link,
            file_type: this.state.file_type,
            upload_time: up_time,
            uid: firebase.auth().currentUser.uid
          }
          const upload = firebase.database().ref().child('files').push(data);
          const id = upload.getKey();
          upload.update({
            id: id
          }).then(() => {
            this.setState({
              link: link,
              file_id: id
            });
          });
        });
      } else {
        swal({
          title: "Failed to upload", 
          text: "Incorect file type",
          icon: "error",
          timer: 2000
        })
        .then(() => {
          window.location.reload()
        })
      }
    })
    .catch((error) => {
      swal({
        title: "Failed to upload", 
        text: `${error}`, 
        icon: "error",
        timer: 2000
      })
      .then(() => {
        window.location.reload()
      })
    })
  }

  sendRequest = () => {
    var data = {
      user_id: firebase.auth().currentUser.uid,
      service_id: String(),
      file_link: this.state.link,
      file_name: this.file.name,
      file_time: this.state.up_time,
      quantity: this.state.quantity,
      total_page: this.state.total_page,
      budget: this.state.budget,
      type: this.state.type,
      status: "PENDING",
      payment_status: "PENDING",
      score_id: String(),
      prices: String(),
      receipt: String(),
      receipt_name: String(),
      reject_description: String(),
      latitude: this.state.latitude,
      longitude: this.state.longitude
    }

    firebase.database().ref().child('requests').push(data)
    .then(() => {
      swal({
        title: "Send request success", 
        text: "Send request success wait for service side to accept file.", 
        timer: 2000,
        icon: "success"
      }).then(() => {
        window.location.replace('/');
      })
    }).catch((error) => {
      swal({
        title: "Send request faild", 
        text: `${error}`, 
        icon: "error"
      })
    });
  }

  setUntilUpload = (status) => {
    this.setState({untilUpload: status});
  }

  setUploadStatus = (status) => {
    this.setState({isUpload: status});
  }

  renderPreview = () => {
    if (this.state.isUpload) {
      if (this.state.link && this.state.file_id && this.state.file_type) {
        return (
          <Preview 
            link={this.state.link} 
            type={this.state.file_type}
            owner={this.state.owner}
            name={this.file.name.split('.')[0]}
            uid={firebase.auth().currentUser.uid}
            time={this.state.up_time}
            file_id={this.state.file_id}
            openModal={this.openModal}
          />
        )
      }
    }
  }

  render() {
    const { open } = this.state;
    return (
      <div>
        <div id="upload-field" className="upload-border" hidden={this.state.isUpload}>
          <h5><strong>Upload file</strong></h5>
          <div className="upload-field mt-4">
            <input id="file-upload" type="file" className="form-control-file in-upload-field"
              aria-describedby="fileHelp" style={{color: this.state.isFileIn ? '#000' : '#aaa'}}
              accept="application/pdf" onChange={this.onFileSelected}/>
          </div>
          <button hidden={this.state.untilUpload} type="button" className="btn btn-light mt-3" 
            onClick={this.uploadFile}><FaUpload/> Uploads</button>
          <div hidden={!this.state.untilUpload} className="my-progress mt-4">
            <div id="my-bar"/>
          </div>
        </div>
        {this.renderPreview()}
        <Modal show={open} onHide={this.closeModal}>
          <div className="modal-bg">
            <Modal.Header>
              <Modal.Title style={{color: 'white'}}><strong>Send Request</strong></Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-bg">
              <div className="send-request-item row">
                <div className="send-request-head">Owner Name :</div>
                <div className="send-request-body">: {this.state.owner}</div>
              </div>
              <div className="send-request-item row">
                <div className="send-request-head">File Name</div>
                <div className="send-request-body">: {this.file ? this.file.name.split('.')[0] : String()}</div>
              </div>
              <div className="send-request-item row">
                <div className="send-request-head">Copy</div>
                <div className="send-request-body">:
                  <input type="number" className="send-request-quantity ml-1" name="quantity" 
                    value={this.state.quantity} onChange={this.handleNumber}/>
                </div>
              </div>
              <div className="send-request-item row">
                <div className="send-request-head">Max Budget</div>
                <div className="send-request-body">: 
                  <input type="number" className="send-request-budget ml-1" name="budget" 
                    value={this.state.budget} onChange={this.handleNumber}/> THB
                </div>
              </div>
              <div className="send-request-item row">
                <div className="send-request-head">Printing Type</div>
                <div className="send-request-body">:
                  <select name="type" className="ml-1" value={this.state.type} onChange={this.handleChange}>
                    <option value="MONO">Mono</option>
                    <option value="COLOR">Color</option>
                  </select>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <button type="button" className="btn btn-secondary" onClick={this.closeModal}><strong>Close</strong></button>
              <button type="button" className="btn btn-primary" onClick={this.sendRequest}><strong>Send</strong></button>
            </Modal.Footer>
          </div>
        </Modal>
      </div>
    );
  }

  closeModal = () => {
    this.setState({open: false})
  }

  openModal = () => {
    this.setState({open: true})
  }
}

export default Upload;