import React, { Component } from 'react';
import firebase from 'firebase';
import * as validate from '../../../../libs/validate';
import { Spinner } from 'reactstrap';
import './FileList.css';

import List from '../item/FileItem';

class FileList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      files: null
    }
  }

  componentDidMount() {
    this.getUser()
    this.getFile()
  }

  getUser = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({user: user})
      }
    })
  }

  getFile = () => {
    // eslint-disable-next-line
    var file = Array();
    firebase.database().ref().child('files')
    .once('value').then((snap) => {
      if (snap.val()) {
        Object.keys(snap.val()).map(key => {
          if (validate.equalsIgnoreCase(snap.val()[key].uid, this.state.user.uid)) {
            file.push(snap.val()[key])
          }
          return (null)
        });
      }
    })
    .then(() => {
      this.setState({files: file})
    })
  }

  render() {
    return (
      <div className="file-container">
        <div className="file-border">
          <h4 className="file-logo">File List</h4>
          <div className="file-list">
            {
              this.state.files ? 
              (
                this.state.files.length > 0 ? 
                (
                  Object.values(this.state.files).map((item, key) => {
                    return (<List key={key} file={item}/>)
                  })
                ) :
                (null)
              ) :
              <Spinner className="mt-2" color="light" />
            }
            
          </div>
        </div>
      </div>
    );
  }
}

export default FileList;