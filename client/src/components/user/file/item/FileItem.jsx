import React, { Component } from 'react';
import firebase from 'firebase';
import axios from 'axios';
import swal from 'sweetalert';
import { FaChevronDown, FaChevronUp} from 'react-icons/fa';
import Modal from 'react-bootstrap/Modal';
import { Collapse } from 'reactstrap';
import * as validate from '../../../../libs/validate';
import { path } from '../../../../resources/DataSet'
import './FileItem.css';

class FileItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
      openCollapse: false,
      date: null,
      owner: null,
      quantity: 1,
      budget: 0,
      type: 'MONO',
      latitude: 0,
      longitude: 0
    }
  }

  componentDidMount() {
    this.convertTime()
    this.getOwner()
  }

  getOwner = () => {
    const { file } = this.props;
    firebase.database().ref().child('users').child(file.uid)
    .once('value').then((snap) => {
      if (snap.val()) {
        this.setState({owner: snap.val()})
      }
    })
    .then(() => {
      navigator.geolocation.getCurrentPosition(location => {
        this.setState({
          latitude: parseFloat(location.coords.longitude),
          longitude: parseFloat(location.coords.latitude)
        })
      })
    })
  }

  convertTime = () => {
    const { file } = this.props;
    const date = new Date(parseInt(file.upload_time)).toLocaleDateString("en-GB")
    this.setState({date});
  }

  handleChange = (event) => {
    this.setState({[event.target.name] : event.target.value});
  }

  handleNumber = (event) => {
    var value = parseInt(event.target.value);
    if (validate.equalsIgnoreCase(event.target.name, 'quantity')) {
      if (value < 1 || isNaN(value)) {
        value = 1;
      }
    } else {
      if (value < 1 || isNaN(value)) {
        value = 0;
      }
    }
    this.setState({[event.target.name]: value});
  }

  deleteFile = () => {
    const { file } = this.props;
    var hasRequest = false;
    swal({
      title: "Are you sure?",
      text: "Reject payment receipt.",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((res) => {
      if (res) {
        firebase.database().ref().child('requests')
        .once('value').then((snap) => {
          if (snap.val()) {
            Object.keys(snap.val()).map(key => {
              if (validate.equals(snap.val()[key].file_link, file.file_link)) {
                hasRequest = true;
              }
              return (null)
            })
          }
        })
        .then(() => {
          if (hasRequest) {
            swal({
              title: "Failed to delete",
              text: "This file have request.",
              icon: "error",
              timer: 2000
            })
          } else {
            firebase.database().ref().child('files').child(file.id)
            .remove().then(() => {
              firebase.storage().ref().child('files').child(file.upload_time).child(file.file_name)
              .delete().then(() => {
                swal({
                  title: "Delete file success",
                  icon: "success",
                  timer: 2000
                })
                .then(() => {
                  window.location.reload()
                })
              })
            })
          }
        })
      }
    })
  }

  sendRequest = () => {
    const { file } = this.props;
    var data;
    axios.post(`${path}/pdf-title`, {
      'link': file.file_link
    }).then((res) => {
      data = {
        user_id: firebase.auth().currentUser.uid,
        service_id: String(),
        file_link: file.file_link,
        file_name: file.file_name,
        file_time: file.upload_time,
        quantity: this.state.quantity,
        total_page: res.data.totalPage,
        budget: this.state.budget,
        type: this.state.type,
        status: "PENDING",
        payment_status: "PENDING",
        score_id: String(),
        prices: String(),
        receipt: String(),
        receipt_name: String(),
        reject_description: String(),
        latitude: this.state.latitude,
        longitude: this.state.longitude
      }
    }).then(() => {
      firebase.database().ref().child('requests').push(data)
      .then(() => {
        swal({
          title: "Send request success", 
          text: "Send request success wait for service side to accept file.", 
          timer: 2000,
          icon: "success"
        }).then(() => {
          window.location.replace('/');
        })
      }).catch((error) => {
        swal({
          title: "Send request faild", 
          text: `${error}`, 
          icon: "error"
        })
      });
    });
  }

  render() {
    const { openModal, openCollapse } = this.state;
    const { file } = this.props;
    return (
      <div className="file-item mt-2 ml-2 row">
        <div className="file-item-title mt-1" onClick={this.statusCollapse}>
          {file.file_name}
        </div>
        <div className="file-item-date" onClick={this.statusCollapse}>
          Date <br/> 
          <div className="date-value">{this.state.date}</div>
        </div>
        <div className="item-arrow mt-2" onClick={this.statusCollapse}>
          { openCollapse ? <FaChevronUp/> : <FaChevronDown/> }
        </div>
        <Collapse className="collapse-area mt-2" isOpen={openCollapse}>
          <div className="border-file">
          <embed title={file.file_name} className="file-show" src={file.file_link} 
            scrolling="yes"/>
          </div>
        </Collapse>
        {
          openCollapse ? 
          (
            <div className="row ml-1 mt-1">
              <button type="button" className="send-file-request mr-1" onClick={this.openModal}>Send Request</button>
              <button type="button" className="file-delete-btn" onClick={this.deleteFile}>Delete</button>
            </div>
          ) :
          (null)
        }
        <Modal show={openModal} onHide={this.closeModal}>
          <div className="modal-bg">
            <Modal.Header>
              <Modal.Title style={{color: 'white'}}><strong>Send Request</strong></Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-bg">
              <div className="send-request-item row">
                <div className="send-request-head">Owner Name :</div>
                <div className="send-request-body">: {this.state.owner ? this.state.owner.name : String()}</div>
              </div>
              <div className="send-request-item row">
                <div className="send-request-head">File Name</div>
                <div className="send-request-body">: {file.file_name}</div>
              </div>
              <div className="send-request-item row">
                <div className="send-request-head">Copy</div>
                <div className="send-request-body">:
                  <input type="number" className="send-request-quantity ml-1" name="quantity" 
                    value={this.state.quantity} onChange={this.handleNumber}/>
                </div>
              </div>
              <div className="send-request-item row">
                <div className="send-request-head">Max Budget</div>
                <div className="send-request-body">: 
                  <input type="number" className="send-request-budget ml-1" name="budget" 
                    value={this.state.budget} onChange={this.handleNumber}/> THB
                </div>
              </div>
              <div className="send-request-item row">
                <div className="send-request-head">Printing Type</div>
                <div className="send-request-body">:
                  <select name="type" className="ml-1" value={this.state.type} onChange={this.handleChange}>
                    <option value="MONO">Mono</option>
                    <option value="COLOR">Color</option>
                  </select>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <button type="button" className="btn btn-secondary" onClick={this.closeModal}><strong>Close</strong></button>
              <button type="button" className="btn btn-primary" onClick={this.sendRequest}><strong>Send</strong></button>
            </Modal.Footer>
          </div>
        </Modal>
      </div>
    );
  }

  statusCollapse = () => {
    this.setState({openCollapse: !this.state.openCollapse});
  }

  closeModal = () => {
    this.setState({openModal: false})
  }

  openModal = () => {
    this.setState({openModal: true})
  }
}

export default FileItem;