import React, { Component } from 'react';
import * as validate from '../../../../libs/validate';
import { Spinner } from 'reactstrap';
import firebase from 'firebase';
import './StatusList.css';

import Item from '../item/StatusItem.jsx';

class RequestStatus extends Component {
  constructor(props) {
    super(props)
    this.state = {
      status: null
    }
  }

  componentDidMount() {
    this.getStatus()
  }

  getStatus = () => {
    // eslint-disable-next-line
    const request = Array();
    firebase.auth().onAuthStateChanged((user) => {
      firebase.database().ref().child('requests')
      .once('value').then((snap) => {
        if (snap.val()) {
          Object.keys(snap.val()).map(key => {
            if (validate.equals(snap.val()[key].user_id, user.uid)) {
              var status = Object.assign({}, snap.val()[key], {id: key})
              request.push(status);
            }
            return (null)
          })
        }
      })
      .then(() => {
        this.setState({status: request})
      })
    })
  }

  render() {
    return (
      <div className="status-container">
        <div className="status-border">
          <h4 className="status-logo">Send Request Status</h4>
          <div className="status-list">
            {
              this.state.status ? 
                (
                  this.state.status.length > 0 ? 
                    Object.values(this.state.status).map((item, key) => {
                      return (
                        <Item key={key} status={item}/>
                      );
                    }) : 
                  (null)
                ) : 
              <Spinner className="mt-2" color="light" />
            }
          </div>
        </div>
      </div>
    );
  }
}

export default RequestStatus;