import React, { Component } from 'react';
import firebase from 'firebase';
import $ from 'jquery';
import swal from 'sweetalert';
import Modal from 'react-bootstrap/Modal';
import StarRatingComponent from 'react-star-rating-component';
import StarRatings from 'react-star-ratings';
import { Collapse } from 'reactstrap';
import { FaChevronDown, FaChevronUp } from 'react-icons/fa';
import * as validate from '../../../../libs/validate';
import * as calculate from '../../../../libs/calculate';
import './StatusItem.css';

class StatusItem extends Component {
  constructor(props) {
    super(props)
    this.receiptImg = null;
    this.state = {
      untilUpload: false,
      openModal: false,
      openCollapse: false,
      user: null,
      service: null,
      serviceUser: null,
      imgPath: null,
      serviceModal: false,
      scoreModal: false,
      fast: 0,
      cost: 0,
      quality: 0,
      comment: String(),
      service_fast: 0,
      service_cost: 0,
      service_quality: 0
    }
  }

  componentDidMount() {
    this.getData();
    this.getServiceScore()
  }

  handleChange = (event) => {
    this.setState({[event.target.name] : event.target.value})
  }

  getData = () => {
    const { status } = this.props;
    firebase.database().ref().child('users').child(status.user_id)
    .once('value').then((snap) => {
      this.setState({user: snap.val()})
    })
    if (validate.notEmpty(status.service_id)) {
      firebase.database().ref().child('services').child(status.service_id)
      .once('value').then((snap) => {
        var serviceItem = Object.assign({}, snap.val());
        firebase.database().ref().child('users').child(status.service_id)
        .once('value').then((snap) => {
          serviceItem = Object.assign({}, serviceItem, {tel: snap.val().tel});
          this.setState({service: serviceItem})
        })
      })
      firebase.database().ref().child('users').child(status.service_id)
      .once('value').then((snap) => {
        this.setState({serviceUser: snap.val()})
      })
    }
    if (validate.notEmpty(status.receipt)) {
      this.setState({imgPath: status.receipt})
    }
    if (validate.notEmpty(status.score_id)) {
      firebase.database().ref().child('scores').child(status.score_id)
      .once('value').then((snap) => {
        this.setState({
          fast: snap.val().service_fast,
          cost: snap.val().service_cost,
          quality: snap.val().service_quality,
          comment: snap.val().comment
        })
      })
    }
  }

  getServiceScore = () => {
    // eslint-disable-next-line
    const fast = Array(), cost = Array(), quality = Array();
    var fast_score = 0, cost_score = 0, quality_score = 0;
    const { status } = this.props;
    firebase.database().ref().child('scores')
    .once('value').then((snap) => {
      if (snap.val()) {
        Object.values(snap.val()).map(item => {
          if (validate.equals(status.service_id, item.service_id)) {
            fast.push(item.service_fast);
            cost.push(item.service_cost);
            quality.push(item.service_quality);
          }
          return (null);
        })
      }
    })
    .then(() => {
      if (fast.length > 0 && cost.length > 0 && quality.length > 0) {
        fast.forEach(score => {
          fast_score += score
        });
        cost.forEach(score => {
          cost_score += score
        });
        quality.forEach(score => {
          quality_score += score
        }); 
      }
    })
    .then(() => {
      if (fast.length > 0 && cost.length > 0 && quality.length > 0) {
        fast_score = (fast_score / fast.length);
        cost_score = (cost_score / cost.length);
        quality_score = (quality_score / quality.length);
      }
      this.setState({
        service_fast: fast_score,
        service_cost: cost_score,
        service_quality: quality_score
      })
    })
  }

  getReceipt = (event) => {
    const { status } =this.props;
    const file = event.target.files[0];
    if (!validate.notEmpty(status.receipt)) {
      this.receiptImg = file;
      this.setState({
        imgPath: URL.createObjectURL(file)
      });
    } else {
      swal({
        title: "Are you sure?",
        text: `Remove payment receipt`,
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((res) => {
        if (res) {
          this.receiptImg = file;
          this.setState({
            imgPath: URL.createObjectURL(file)
          });
        }
      });
    }
  }

  clearReceipt = () => {
    const { status } = this.props;
    if (!validate.notEmpty(status.receipt)) {
      this.receiptEmpty()
    } else {
      swal({
        title: "Are you sure?",
        text: `Remove payment receipt`,
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((res) => {
        if (res) {
          this.canclePayment();
        }
      });
    }
  }

  canclePayment = () => {
    const { status } = this.props;
    firebase.database().ref().child('requests').child(status.id)
    .update({
      receipt: String(),
      receipt_name: String(),
      reject_description: String(),
      payment_status: 'PENDING'
    })
    .then(() => {
      firebase.storage().ref().child('receipts').child(status.id).child(status.receipt_name)
      .delete().then(() => {
        swal({
          title: "Remove payment receipt success",
          icon: "success",
          timer: 2000,
        })
        .then(() => {
          window.location.reload();
        })
      })
    })
  }

  receiptEmpty = () => {
    this.receiptImg = null;
    this.setState({imgPath: null}, () => {
      $("#receipt").val('');
    })
  }

  renderMakePayment = () => {
    if (this.state.imgPath && this.receiptImg) {
      return (<button type="button" className="btn btn-primary" onClick={this.makePayment}><strong>Make Payment</strong></button>);
    } else {
      return (<button type="button" className="btn btn-primary" disabled><strong>Make Payment</strong></button>);
    }
  }

  makePayment = () => {
    const { status } = this.props;
    if (validate.notEmpty(status.receipt)) {
      firebase.storage().ref().child('receipts').child(status.id).child(status.receipt_name).delete()
    }
    this.uploadReceipts()
  }

  uploadReceipts = () => {
    const { status } = this.props;
    const uploadTask = firebase.storage().ref().child('receipts').child(status.id).child(this.receiptImg.name).put(this.receiptImg);
    uploadTask.on('state_changed', (snapshot) => {
      var progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
      this.setUntilUpload(true);
      var elem = document.getElementById("my-bar");
      elem.style.width = progress + '%';
    }, (error) => {
      swal({
        title: "Failed to upload receipts", 
        text: `${error}`, 
        icon: "error"
      })
    }, () => {
      uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
        this.setUntilUpload(false);
        this.updateRequest(downloadURL);
      });
    });
  }

  payByCash = () => {
    const { status } = this.props;
    firebase.database().ref().child('requests').child(status.id)
    .update({
      receipt: String(),
      receipt_name: String(),
      reject_description: String(),
      payment_status: 'CASH'
    })
    .then(() => {
      const date = new Date();
      const history = {
        customer_id: status.user_id,
        service_id: status.service_id,
        income: status.prices,
        time: date.getTime()
      }
      firebase.database().ref().child('historys').child(status.service_id).child(status.id)
      .set(history).then(() => {
        swal({
          title: "Make payment success",
          icon: "success",
          timer: 2000,
        })
        .then(() => {
          window.location.reload();
        })
      })
    })
  }

  updateRequest = (link) => {
    const { status } = this.props;
    firebase.database().ref().child('requests').child(status.id)
    .update({
      receipt: link,
      receipt_name: this.receiptImg.name,
      reject_description: String(),
      payment_status: 'PAID'
    })
    .then(() => {
      const history = {
        customer_id: status.user_id,
        service_id: status.service_id,
        income: status.prices,
        time: new Date().getTime()
      }
      firebase.database().ref().child('historys').child(status.service_id).child(status.id)
      .set(history).then(() => {
        swal({
          title: "Make payment success",
          icon: "success",
          timer: 2000,
        })
        .then(() => {
          window.location.reload();
        })
      })
    })
  }

  deleteRequest = () => {
    const { status } = this.props;
    swal({
      title: "Are you sure?",
      text: "Remove request",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((res) => {
      if (res) {
        firebase.database().ref().child('requests').child(status.id)
        .remove().then(() => {
          swal({
            title: "Delete request success",
            icon: "success",
            timer: 2000,
          })
          .then(() => {
            window.location.reload();
          });
        });
      }
    });
  }

  rateScore = () => {
    const { fast, cost, quality, comment } = this.state;
    const { status } = this.props;
    const zero = (
      validate.equalsNumber(fast, 0) ||
      validate.equalsNumber(cost, 0) ||
      validate.equalsNumber(quality, 0)
    );
    if (!zero) {
      const score = {
        service_fast: fast,
        service_cost: cost,
        service_quality: quality,
        comment: comment,
        customer_id: firebase.auth().currentUser.uid,
        service_id: status.service_id,
        time: new Date().getTime()
      }
      const rateUpload = firebase.database().ref().child('scores').push(score)
      const score_id = rateUpload.getKey()
      firebase.database().ref().child('requests').child(status.id).update({
        score_id: score_id
      })
      .then(() => {
        swal({
          title: "Rate score success.",
          icon: "success",
          timer: 2000
        })
        .then(() => {
          window.location.reload();
        })
      })
    } else {
      swal({
        title: `Please rate score of ${
          validate.equalsNumber(fast, 0) ? "service's fast" : 
          validate.equalsNumber(cost, 0) ? "services cost" : "service's quality"
        }.`,
        icon: "warning"
      })
    }
  }

  setUntilUpload = (status) => {
    this.setState({untilUpload: status});
  }

  render() {
    const { openModal, openCollapse, service, serviceModal, scoreModal } = this.state;
    const { status } = this.props;
    return (
      <div className="status-item mt-2 ml-2 row">
        <div className="item-title" onClick={this.statusCollapse}>
          {status.file_name}
        </div>
        <div className="item-status" onClick={this.statusCollapse}>
          <div 
            style={{
              color: (
                validate.equalsIgnoreCase(status.status, 'PENDING') ? 'gray' : 
                validate.equalsIgnoreCase(status.status, 'WAIT_PAYMENT') ? 'yellow' :
                validate.equalsIgnoreCase(status.status, 'IN_PROGRESS') ? 'aqua' :
                validate.equalsIgnoreCase(status.status, 'COMPLETE') ? 'green' : 
                validate.equalsIgnoreCase(status.status, 'REJECT') ? 'red' : 'white'
              )
            }}
          >
            <strong>{status.status.replace("_", " ")}</strong>
          </div>
        </div>
        <div className="item-arrow" onClick={this.statusCollapse}>
          { openCollapse ? <FaChevronUp/> : <FaChevronDown/> }
        </div>
        <Collapse className="collapse-area mt-2" isOpen={openCollapse}>
          <div className="collapse-item row mt-2">
            <div className="collapse-item-head ml-1"><strong>User Name</strong></div>
            <div className="collapse-item-item">: {this.state.user ? this.state.user.name : String()}</div>
          </div>
          {
            service ? 
            (
              <div className="collapse-item row">
                <div className="collapse-item-head ml-1"><strong>Service Name</strong></div>
                <div className="collapse-item-item">: 
                  {
                    // eslint-disable-next-line
                    <a className="ml-1" onClick={this.serviceModal}><u className="service-info">{service.name}</u></a>
                  }
                </div>
              </div>
            ) :
            (null)
          }
          <div className="collapse-item row">
            <div className="collapse-item-head ml-1"><strong>Copy</strong></div>
            <div className="collapse-item-item">: {status.quantity}</div>
          </div>
          {
            validate.notEmpty(status.prices) ?
            (
              <div className="collapse-item row">
                <div className="collapse-item-head ml-1"><strong>Prices</strong></div>
                <div className="collapse-item-item">: {status.prices} THB</div>
              </div>
            ) : 
            (null)
          }
          {
            validate.equalsIgnoreCase(status.status, 'WAIT_PAYMENT') ? 
            (
              validate.equalsIgnoreCase(status.payment_status, 'PENDING') ? 
              (
                <div className="collapse-item row">
                  <div className="collapse-item-head ml-1 mt-2"><strong>Confirm Payment</strong></div>
                  <div className="collapse-item-item">: 
                    <button type="button" className="save-price ml-2" onClick={this.openModal}>Upload Receipt</button>
                    <button type="button" className="save-price ml-2" onClick={this.payByCash}>Pay By Cash</button>
                  </div>
                </div> 
              ) : 
              (
                <div className="collapse-item row">
                  <div className="collapse-item-head ml-1"><strong>Payment Status</strong></div>
                  <div className="collapse-item-item">: 
                    <strong className={`${validate.equalsIgnoreCase(status.payment_status, 'PAID') ? 'paid-review ': 
                      validate.equalsIgnoreCase(status.payment_status, 'REJECT') ? 'reject-preview' : String()} ml-1`} 
                      style={{color: validate.equalsInSet(status.payment_status, ['PAID', 'CASH']) ? 'green' : 'red'}} 
                      onClick={validate.equalsInSet(status.payment_status, ['PAID', 'REJECT']) ?  this.openModal : null}
                    >
                      {status.payment_status}
                    </strong>
                  </div>
                </div>
              )
            ) :
            (null)
          }
          {
            validate.notEmpty(status.reject_description) ? 
            (
              <div className="collapse-item row">
                <div className="collapse-item-head ml-1"><strong>Reject Description</strong></div>
                <div className="collapse-item-item">: {status.reject_description}</div>
              </div>
            ) : 
            (null)
          }
          <div className={`collapse-item row ${validate.equalsIgnoreCase(status.status, 'COMPLETE') ? String() : 'mb-2'}`}>
            <div className="collapse-item-head ml-1"><strong>Color Request</strong></div>
            <div className="collapse-item-item">: {status.type}</div>
          </div>
          {
            validate.equalsIgnoreCase(status.status, 'COMPLETE') ?
            (
              <div className="collapse-item row mb-2">
                <div className="collapse-item-head ml-1"><strong>Rate Service</strong></div>
                <div className="collapse-item-item">: 
                  {
                    validate.notEmpty(status.score_id) ? 
                    (<strong className="view-score ml-1" style={{color: 'green'}} onClick={this.scoreModal}>Already rate service's score.</strong>) :
                    // eslint-disable-next-line
                    (<a className="ml-1" onClick={this.scoreModal}><u className="rate-btn">Click for rate service's score</u></a>)
                  }
                </div>
              </div>
            ) : 
            (null)
          }
        </Collapse>
        <Modal show={openModal} onHide={this.closeModal}>
          <div className="modal-bg">
            <Modal.Header>
              <Modal.Title style={{color: 'white'}}><strong>Payment</strong></Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-bg">
              <div className="input-group mb-3 mt-3">
                <div className="input-group-prepend">
                  <span className="input-group-text"><strong>Upload Receipt</strong></span>
                </div>
                <input id="receipt" type="file" className="form-control" onChange={this.getReceipt}
                  style={{color: this.receiptImg ? '#000' : '#aaa'}}/>
              </div>
              {
                service && !this.state.imgPath ?
                (
                  validate.notEmpty(service.bank_name) ?
                  (
                    <div className="payment-info-item row">
                      <div className="payment-info-head">Bank</div>
                      <div className="payment-info-body">: {service.bank_name}</div>
                    </div>
                  ) :
                  (null)
                ) :
                (null)
              }
              {
                service && !this.state.imgPath ? 
                (
                  validate.notEmpty(service.account_number) ?
                  (
                    <div className="payment-info-item row">
                      <div className="payment-info-head">Account Number</div>
                      <div className="payment-info-body">: {service.account_number}</div>
                    </div>
                  ) : 
                  (null)
                ) : 
                (null)
              }
              {
                service && !this.state.imgPath ? 
                (
                  validate.notEmpty(service.promptpay) ?
                  (
                    <div className="payment-info-item row">
                      <div className="payment-info-head">Promptpay</div>
                      <div className="payment-info-body">: {service.promptpay}</div>
                    </div>
                  ) : 
                  (null)
                ) : 
                (null)
              }
              {
                service && !this.state.imgPath ? 
                (
                  validate.notEmpty(service.QRcode_link) ?
                  (
                    <div className="img-border">
                      <img src={service.QRcode_link} alt="QR_Code" width="100%"/>
                    </div>
                  ) :
                  (null)
                ) :
                (null)
              }
              {
                this.state.imgPath ? 
                (
                  <div>
                    <button type="button" className="clear-img ml-2 mb-2" onClick={this.clearReceipt}>Clear</button>
                    <div className="img-border">
                      <img src={this.state.imgPath} alt="receipt" width="100%"/>
                    </div>
                    <div hidden={!this.state.untilUpload} className="my-progress mt-4">
                      <div id="my-bar"/>
                    </div>
                  </div>
                ) :
                (null)
              }
            </Modal.Body>
            <Modal.Footer>
              <button type="button" className="btn btn-secondary" onClick={this.closeModal}><strong>Close</strong></button>
              {this.renderMakePayment()}
            </Modal.Footer>
          </div>
        </Modal>
        <Modal show={serviceModal} onHide={this.serviceModal}>
          <div className="modal-bg">
            <Modal.Header>
              <Modal.Title style={{color: 'white'}}><strong>Service Information</strong></Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-bg">
              <div className="payment-info-item mb-2 row">
                <div className="payment-info-head">Service Name</div>
                <div className="payment-info-body">: 
                  {
                    service ? 
                     (
                       validate.notEmpty(service.name) ? 
                       ` ${service.name}` : 
                       ' -'
                    ) : 
                    String()
                  }
                </div>
              </div>
              <div className="payment-info-item mb-2 row">
                <div className="payment-info-head">Email</div>
                <div className="payment-info-body">: 
                  {
                    service ? 
                     (
                       validate.notEmpty(service.email) ? 
                       ` ${service.email}` : 
                       ' -'
                    ) : 
                    String()
                  }
                </div>
              </div>
              <div className="payment-info-item mb-2 row">
                <div className="payment-info-head">Telephone Number</div>
                <div className="payment-info-body">: 
                  {
                    service ? 
                     (
                       validate.notEmpty(service.tel) ? 
                       ` ${service.tel}` : 
                       ' -'
                    ) : 
                    String()
                  }
                </div>
              </div>
              <div className="payment-info-item row">
                <div className="payment-info-head">Service Address</div>
                <div className="payment-info-body">: 
                  {
                    service ? 
                     (
                       validate.notEmpty(service.address) ? 
                       ` ${service.address}` : 
                       ' -' 
                    ) : 
                    String()
                  }
                </div>
              </div>
              <div className="payment-info-head mt-3" style={{width: '80%', textAlign: 'center'}}>Service's Score</div>
              <div className="payment-info-item row mt-3">
                <div className="payment-info-head">Service's Fast</div>
                <div className="payment-info-star">
                  <StarRatings
                    rating={this.state.service_fast}
                    starRatedColor="rgb(255, 180, 0)"
                    numberOfStars={5}
                    starDimension="20px"
                  />
                </div>
                <div className="payment-info-emoji">{calculate.emoji(this.state.service_fast).emoji} {calculate.emoji(this.state.service_fast).value}</div>
              </div>
              <div className="payment-info-item row mt-2">
                <div className="payment-info-head">Services Cost</div>
                <div className="payment-info-star">
                  <StarRatings
                    rating={this.state.service_cost}
                    starRatedColor="rgb(255, 180, 0)"
                    numberOfStars={5}
                    starDimension="20px"
                  />
                </div>
                <div className="payment-info-emoji">{calculate.emoji(this.state.service_cost).emoji} {calculate.emoji(this.state.service_cost).value}</div>
              </div>
              <div className="payment-info-item row mt-2">
                <div className="payment-info-head">Service's Quality</div>
                <div className="payment-info-star">
                  <StarRatings
                    rating={this.state.service_quality}
                    starRatedColor="rgb(255, 180, 0)"
                    numberOfStars={5}
                    starDimension="20px"
                  />
                </div>
                <div className="payment-info-emoji">{calculate.emoji(this.state.service_quality).emoji} {calculate.emoji(this.state.service_quality).value}</div>
              </div>
            </Modal.Body>
            <Modal.Footer/>
          </div>
        </Modal>
        <Modal show={scoreModal} onHide={this.scoreModal}>
          <div className="modal-bg">
            <Modal.Header>
              <Modal.Title style={{color: 'white'}}><strong>Rate Score</strong></Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-bg">
              <div className="payment-info-item mt-2 row">
                <div className="payment-info-head">Service's Fast</div>
                <div className="payment-info-body">
                    <StarRatingComponent 
                      name="fast" 
                      starCount={5}
                      value={this.state.fast}
                      onStarClick={validate.notEmpty(status.score_id) ? (null) : this.onStarClick}
                    />
                </div>
              </div>
              <div className="payment-info-item row">
                <div className="payment-info-head">Services Cost</div>
                <div className="payment-info-body">
                  <StarRatingComponent 
                    name="cost" 
                    starCount={5}
                    value={this.state.cost}
                    onStarClick={validate.notEmpty(status.score_id) ? (null) : this.onStarClick}
                  />
                </div>
              </div>
              <div className="payment-info-item row">
                <div className="payment-info-head">Service's Quality</div>
                <div className="payment-info-body">
                  <StarRatingComponent 
                    name="quality" 
                    starCount={5}
                    value={this.state.quality}
                    onStarClick={validate.notEmpty(status.score_id) ? (null) : this.onStarClick}
                  />
                </div>
              </div>
              <div className="payment-info-item row">
                <textarea className="comment-area mt-2 ml-4" name="comment" value={this.state.comment}
                  onChange={this.handleChange} placeholder="Comment" readOnly={validate.notEmpty(status.score_id)}/>
              </div>
            </Modal.Body>
            <Modal.Footer>
              {
                validate.notEmpty(status.score_id) ? 
                (null) :
                (<button type="button" className="btn btn-primary" onClick={this.rateScore}><strong>Rate Score</strong></button>)
              }
            </Modal.Footer>
          </div>
        </Modal>
        {
          openCollapse && validate.equalsIgnoreCase(status.status, 'PENDING') ? 
          (
            <div className="ml-1 mt-2 row" style={{marginBottom: '-8px'}}>
              <button type="button" className="delete-btn-request mr-1" onClick={this.deleteRequest}>Delete</button>
            </div>
          ) : 
          (null)
        }
      </div>
    );
  }

  onStarClick = (nextValue, prevValue, name) => {
    if (!validate.equalsNumber(prevValue, nextValue)) {
      this.setState({[name] : nextValue})
    }
  }

  statusCollapse = () => {
    this.setState({openCollapse: !this.state.openCollapse})
  }

  scoreModal = () => {
    this.setState({scoreModal: !this.state.scoreModal})
  }

  serviceModal = () => {
    this.setState({serviceModal: !this.state.serviceModal})
  }

  openModal = () => {
    this.setState({openModal: true})
  }

  closeModal = () => {
    this.setState({openModal: false})
  }
}

export default StatusItem;