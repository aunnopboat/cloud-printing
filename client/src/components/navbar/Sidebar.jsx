import React, { Component } from 'react';
import swal from 'sweetalert';
import { AVATAR } from '../../resources/images/user-avatar/avatar-item';
import * as validate from '../../libs/validate';
import firebase from 'firebase';
import { 
  FaUserAlt, 
  FaSignInAlt, 
  FaSignOutAlt, 
  FaRegRegistered, 
  FaBars,
  FaUpload,
  FaMapMarkerAlt,
  FaFile,
  FaClipboardList,
  FaClipboardCheck,
  FaComments,
  FaChartBar,
  FaStarHalfAlt,
  FaRegChartBar } from 'react-icons/fa';
import './Sidebar.css';

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      displayStatus: 'none',
      role: null,
      name: String(),
      username: String(),
      picture: 0
    }
  }

  componentDidMount() {
    this.getUser();
  }

  getUser() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({user: user}, () => {
          firebase.database().ref().child('users').child(this.state.user.uid).once('value')
          .then((snap) => {
            if (snap.val()) {
              if (!snap.val().ban_user) {
                this.setState({
                  name: snap.val().name,
                  username: snap.val().username,
                  role: snap.val().user_type,
                  picture: snap.val().picture
                });
              } else {
                swal({
                  title: "Your account has already ban.",
                  icon: "error",
                })
                .then(() => {
                  firebase.auth().signOut()
                  .then(() => {
                    window.location.replace('/login');
                  })
                })
              }
            }
          });
        });
      }
    });
  }

  sidebarOpen = () => {
    this.setState({displayStatus: 'block'});
  }

  sidebarClose = () => {
    this.setState({displayStatus: 'none'});
  }

  signOut() {
    firebase.auth().signOut()
    .then(() => {
      window.location.replace('/login');
    });
  }

  renderUserItem() {
    if (validate.equalsIgnoreCase(this.state.role, 'USER')) {
      return (
        <div>
          <div className="sidebar-item" 
            onClick={() => window.location.replace('/cloud/upload')}>
            <FaUpload/> Upload
          </div>
          <div className="sidebar-item" 
            onClick={() => window.location.replace('/cloud/user/file')}>
            <FaFile/> File
          </div>
          <div className="sidebar-item" 
            onClick={() => window.location.replace('/cloud/request/status')}>
            <FaClipboardList/> Send Request Status
          </div>
        </div>
      );
    }
  }

  renderServiceItem() {
    if (validate.equalsIgnoreCase(this.state.role, 'SERVICE')) {
      return (
        <div>
          <div className="sidebar-item" 
            onClick={() => window.location.replace('/service/request/pending/list')}>
            <FaClipboardList/> Request List
          </div>
          <div className="sidebar-item" 
            onClick={() => window.location.replace('/service/accept/list')}>
            <FaClipboardCheck/> Accept List
          </div>
          <div className="sidebar-item" 
            onClick={() => window.location.replace('/service/incomes')}>
            <FaChartBar/> Incomes
          </div>
          <div className="sidebar-item" 
            onClick={() => window.location.replace('/service/score')}>
            <FaStarHalfAlt/> Score
          </div>
        </div>
      );
    }
  }

  renderDefaultItem() {
    if (!validate.equalsIgnoreCase(this.state.role, 'ADMIN') && this.state.role) {
      return (
        <div className="sidebar-item" 
          onClick={() => {window.location.replace('/cloud/map')}}>
          <FaMapMarkerAlt/> Map
        </div>
      )
    }
  }

  renderUserAvatar() {
    if (validate.notEmpty(this.state.name) && validate.notEmpty(this.state.username)) {
      return (
        <div className="sidebar-user-container">
          <div className="row">
            <div className="sidebar-user-pic">
              <img alt="user avatar" className="user-avatar" src={AVATAR[this.state.picture]}/>
            </div>
            <div>
              <strong>
                {this.state.name}
                <p className="litle-username">{this.state.username}</p>
              </strong>
            </div>
          </div>
        </div>
      );
    }
  }

  renderSideBarBottom() {
    if (!this.state.user) {
      return (
        <div className="row">
          <div className="sidebar-bottom-item" 
            onClick={() => window.location.replace('/register')}>
            <FaRegRegistered/> <br/> Register
          </div>
          <div className="sidebar-bottom-item auth" 
            onClick={() => window.location.replace('/login')}>
            <FaSignInAlt/> <br/> Sign In
          </div>
        </div>
      );
    } else {
      return (
        <div className="row">
          <div className="sidebar-bottom-item"
            onClick={() => window.location.replace('/cloud/user_account')}>
            <FaUserAlt/> <br/> Account
          </div>
          <div className="sidebar-bottom-item auth" 
            onClick={this.signOut}>
            <FaSignOutAlt/> <br/> Sign Out
          </div>
        </div>
      );
    }
  }

  renderAdmin() {
    if (validate.equalsIgnoreCase(this.state.role, 'ADMIN')) {
      return (
        <div>
          <div className="sidebar-item" 
            onClick={() => window.location.replace('/admin/user_list')}>
            <FaClipboardList/> User List
          </div>
          <div className="sidebar-item" 
            onClick={() => window.location.replace('/admin/income_rate')}>
            <FaRegChartBar/> Service Incomes
          </div>
          <div className="sidebar-item" 
            onClick={() => window.location.replace('/admin/report/list')}>
            <FaComments/> Report List
          </div>
        </div>
      );
    }
  }

  renderReport() {
    if (this.state.user && this.state.role && !validate.equalsIgnoreCase(this.state.role, 'ADMIN')) {
      return (
        <div className="sidebar-report-item"
          onClick={() => window.location.replace('/cloud/send/report')}>
          <FaComments/> Report
        </div>
      )
    }
  }

  render() {
    const { displayStatus } = this.state;
    return (
      !validate.equalsIgnoreCase(localStorage['SideBar'], 'hidden') ?
      (
        <div>
          <div className="sidebar-btn-container">
            <FaBars className="sidebar-btn" onClick={this.sidebarOpen} 
              data-toggle="tooltip" title="click for open sidebar"/> 
          </div>
          <div className="sidebar-container" 
            style={{display: displayStatus}} onMouseLeave={this.sidebarClose}>
            <div className="sidebar-logo"><strong>Cloud Printing</strong></div>
            {this.renderUserAvatar()}
            <div className="sidebar-item-container">
              {this.renderUserItem()}
              {this.renderServiceItem()}
              {this.renderAdmin()}
              {this.renderDefaultItem()}
            </div>
            <div className="sidebar-item-bottom">
              {this.renderReport()}
              {this.renderSideBarBottom()}
            </div>
          </div>
        </div> 
      ) : 
      (null)
    )
  }
}

export default Sidebar