import React, { Component } from 'react';
import firebase from 'firebase';
import { Spinner } from 'reactstrap';
import * as validate from '../../../../libs/validate';
import * as filter from '../../../../libs/filter';
import './UserList.css';

import UserItem from '../item/UserItem';
import ServiceItem from '../item/ServiceItem';

class UserList extends Component {
  constructor(props) {
    super(props)
    // eslint-disable-next-line
    this.backUpUser = Array();
    // eslint-disable-next-line
    this.backUpService = Array(); 
    this.state = {
      type: 'USER',
      userList: null,
      serviceList: null,
      admin: null
    }
  }

  componentDidMount() {
    this.getAdmin()
    this.getList();
  }

  getAdmin = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({user: user}, () => {
          firebase.database().ref().child('users').child(this.state.user.uid).once('value')
          .then((snap) => {
            this.setState({admin: snap.val()})
          })
        });
      }
    });
  }

  handleType = (event) => {
    this.setState({type : event.target.value}, () => {
      this.checkType();
    })
  }

  checkType = () => {
    switch (this.state.type) {
      case 'USER': this.setState({userList: this.backUpUser}); break;
      case 'SERVICE': this.setState({serviceList: this.backUpService}); break;
      default:break;
    }
  }

  getList = () => {
    firebase.database().ref().child('users')
    .once('value').then((snap) => {
      if (snap.val()) {
        Object.keys(snap.val()).map(key => {
          switch (snap.val()[key].user_type) {
            case 'USER': this.backUpUser.push(snap.val()[key]); break;
            case 'SERVICE': this.backUpService.push(snap.val()[key]); break;
            default:break;
          }
          return (null)
        });
      }
    })
    .then(() => {
      this.checkType();
    })
  }

  searchUser = (event) => {
    const value = event.target.value
    this.setState({
      userList: null,
      serviceList: null
    }, () => {
      if (validate.notEmpty(value)) {
        var getFilter = null
        if (validate.equalsIgnoreCase(this.state.type, 'USER')) {
          getFilter = filter.filterName(value, this.backUpUser);
          this.setState({userList: getFilter});
        } else {
          getFilter = filter.filterName(value, this.backUpService);
          this.setState({serviceList: getFilter})
        }
      } else {
        if (validate.equalsIgnoreCase(this.state.type, 'USER')) {
          this.setState({userList: this.backUpUser})
        } else {
          this.setState({serviceList: this.backUpService});
        }
      }
    })
  }

  render() {
    return (
      <div className="user-container">
        <div className="user-border">
          <h4 className="user-logo">User List</h4>
          <div className="user-selection-area row">
            <select className="user-type ml-3" value={this.state.type} onChange={this.handleType}>
              <option value="USER">User</option>
              <option value="SERVICE">Service</option>
            </select>
            <input type="text" className="search-user ml-3" placeholder="Search name" onChange={this.searchUser}/>
          </div>
          <div className="user-list-area mt-3">
          {
            validate.equalsIgnoreCase(this.state.type, 'USER') ?
            (
              this.state.userList ? 
              (
                this.state.userList.length > 0 && this.state.admin?
                (
                  Object.values(this.state.userList).map((item, key) => {
                    var object = Object.assign({}, item, {index: (key + 1)})
                    return (
                      <UserItem key={key} item={object} admin={this.state.admin} />
                    );
                  })
                ) : 
                (null)
              ) : 
              <Spinner className="mt-2" color="light" />
            ) : 
            (
              this.state.serviceList ? 
              (
                this.state.serviceList.length > 0 && this.state.admin?
                (
                  Object.values(this.state.serviceList).map((item, key) => {
                    var object = Object.assign({}, item, {index: (key + 1)})
                    return (
                      <ServiceItem key={key} item={object} admin={this.state.admin} />
                    );
                  })
                ) : 
                (null)
              ) : 
              <Spinner className="mt-2" color="light" />
            )
          }
          </div>
        </div>
      </div>
    );
  }
}

export default UserList;