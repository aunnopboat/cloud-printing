import React, { Component } from 'react';
import firebase from 'firebase';
import { Spinner } from 'reactstrap';
import swal from 'sweetalert';
import * as validate from '../../../../libs/validate';

class ServiceItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      item: this.props.item,
      uid: this.props.item.uid,
      type: this.props.item.user_type,
      admin: this.props.admin,
      deleteStatus: false,
    }
  }

  checkDalete = () => {
    swal({
      title: "Are you sure?",
      text: `Delete user "${this.state.item.name}"`,
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((res) => {
      if (res) {
        this.setState({deleteStatus: true}, () => {
          this.serviceDelete();
        })
      }
    });
  }

  serviceDelete = () => {
    const { item, admin } = this.state;
    firebase.auth().signOut()
    .then(() => {
      firebase.auth().signInWithEmailAndPassword(item.email, item.password)
      .then(() => {
        firebase.database().ref().child('logins')
        .once('value').then(snap => {
          if (snap.val()) {
            Object.keys(snap.val()).map(key => {
              if (validate.equals(snap.val()[key].email, item.email)) {
                firebase.database().ref().child('logins').child(key).remove()
              }
              return (null)
            });
          }
        })
        .then(() => {
          firebase.database().ref().child('requests')
          .once('value').then(snap => {
            if (snap.val()) {
              Object.keys(snap.val()).map(key => {
                if (validate.equals(snap.val()[key].service_id, item.uid)) {
                  firebase.database().ref().child('requests').child(key).remove()
                }
                return (null)
              });
            }
          })
          .then(() => {
            firebase.database().ref().child('services').child(item.uid).remove()
            .then(() => {
              firebase.database().ref().child('users').child(item.uid).remove()
              .then(() => {
                firebase.auth().currentUser.delete().then(() => {
                  firebase.auth().signInWithEmailAndPassword(admin.email, admin.password)
                  .then(() => {
                    swal({
                      title: "Delete user success",
                      icon: "success",
                      timer: 2000,
                    })
                    .then(() => {
                      window.location.reload();
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  }

  banUser = () => {
    const { item } = this.state;
    const ban_result = !item.ban_user;
    swal({
      title: "Are you sure?",
      text: `To ${item.ban_user ? 'un ban' : 'ban'} user "${item.name}".`,
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((res) => {
      if (res) {
        firebase.database().ref().child('users').child(item.uid)
        .update({
          ban_user: ban_result
        })
        .then(() => {
          swal({
            title: `${ban_result ? 'Ban' : 'Un ban'} user success`,
            icon: "success",
            timer: 2000,
          })
          .then(() => {
            window.location.reload();
          })
        })
      }
    })
  }

  render() {
    const { item } = this.state;
    return (
      <div className="user-list-item mb-2">
        <div className="user-list-detail row">
          <div className="col-sm-1 user-list-index mt-2">{item.index}</div>
          <div className="col-sm-3 user-list-name mt-2">{item.name}</div>
          <div className="col-sm-3 user-list-detail">Username<br/>{item.username}</div>
          <div className="col-sm-3 user-list-detail">Email<br/>{item.email}</div>
          <div className="row">
            <button type="button" className={`btn-${item.ban_user ? 'unban': 'ban'} mr-1`} onClick={this.banUser}>
              {`${item.ban_user ? 'Un Ban' : 'Ban'}`}
            </button>
            {
              !this.state.deleteStatus ?
              <button type="button" className="btn-delete" onClick={this.checkDalete}>Dalete</button> :
              <Spinner className="mt-2" color="light" />
            }
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceItem;