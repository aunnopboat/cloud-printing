import React, { Component } from 'react';
import firebase from 'firebase';
import { Spinner } from 'reactstrap';
import './ServiceIncomeRate.css';

class ServiceIncomeRate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      requests: null,
      incomes: null,
      max_price: 0
    }  
  }

  componentDidMount() {
    this.getRequest()
  }

  getRequest = () => {
    // eslint-disable-next-line
    var requests = Array()
    firebase.database().ref().child('requests')
    .once('value').then((snap) => {
      // eslint-disable-next-line
      Object.values(snap.val()).map(item => {
        if (item.payment_status === "PAID" || item.payment_status ===  "CASH") {
          requests.push(item)
        }
      })
    })
    .then(() => {
      this.setState({requests: requests});
    })
    .then(() => {
      this.getService()
    })
  }

  getService = () => {
    // eslint-disable-next-line
    var incomes = Array()
    firebase.database().ref().child('users').orderByChild('user_type').equalTo("SERVICE")
    .once('value').then((snap) => {
      // eslint-disable-next-line
      Object.values(snap.val()).map(item => {
        let data = {
          uid: item.uid,
          name: item.name,
          picture: item.picture
        }
        incomes.push(Object.assign({}, data, {income: 0}))
      })
    })
    .then(() => {
      // eslint-disable-next-line
      Object.values(incomes).map((sv, key) => {
        // eslint-disable-next-line
        Object.values(this.state.requests).map(req => {
          if (req.service_id === sv.uid) {
            incomes[key].income += req.prices
          }
        })
      })
    })
    .then(() => {
      incomes = (incomes.sort((a, b) => {
        return (a.income > b.income) ? -1 : (a.income < b.income) ? 1 : 0
      }));
    })
    .then(() => {
      this.setState({incomes: incomes, max_price: incomes[0].income})
    })
  }

  render() {
    return(
      <div className="service-rate-container">
        <div className="service-rate-border">
          <h4 className="service-rate-logo">
            Service Incomes Rate
          </h4>
          <div className="service-rate-list">
            {
              this.state.incomes ? 
                this.state.incomes.length > 0 ? 
                  Object.values(this.state.incomes).map((item, key) => {
                    return(
                      <div className="service-rate-item">
                        <div className="index">{key + 1}</div>
                        <div className="name">{item.name}</div>
                        <div className="price">{item.income.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} Bath</div>
                        <div className="bar">
                          <div className="value" style={{width: `${(item.income * 100) / this.state.max_price}%`}}/>
                        </div>
                      </div>
                    )
                  })
                :null 
              : <Spinner className="mt-2" color="light" />
            }
          </div>
        </div>
      </div>
    )
  }
}

export default ServiceIncomeRate;