import React, { Component } from 'react';
import firebase from 'firebase';
import swal from 'sweetalert';
import * as validate from '../../../../libs/validate';
import './ReportItem.css';

class ReportItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      report: this.props.report,
      sender: null
    }
  }

  componentDidMount() {
    this.getSender()
  }

  getSender = () => {
    const { report } = this.state;
    firebase.database().ref().child('users').child(report.report_sender)
    .once('value').then((snap) => {
      this.setState({sender: snap.val()})
    })
  }

  deleteReport = () => {
    const { report } = this.state;
    firebase.database().ref().child('reports').child(report.id)
    .remove().then(() => {
      swal({
        title: "Delete report success",
        icon: "success",
        timer: 2000,
      })
      .then(() => {
        window.location.reload();
      })
    })
  }

  render() {
    const { report, sender } = this.state;
    return (
      <div className="report-item-border mt-1">
        <div className="report-item-inner-border">
          <div className="report-item"><strong>Report Type</strong><br/>
            <p className="ml-2"> - {validate.equalsIgnoreCase(report.report_type, 'SYSTEM_PROBLEM') ? 'System Problems' : 'User Report'}</p>
          </div>
          <div className="report-item"><strong>Report Sender</strong><br/>
            <p className="ml-2"> - {sender ? sender.name : String()}</p>
          </div>
          <div className="report-item"><strong>Report Detail</strong><br/>
            <p className="ml-2">{report.report_text}</p>
          </div>
        </div>
        <button className="delete-report-btn mt-2" type="button" onClick={this.deleteReport}>Delete</button>
      </div>
    );
  }
}

export default ReportItem;