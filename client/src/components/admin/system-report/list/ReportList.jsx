import React, { Component } from 'react';
import firebase from 'firebase';
import './ReportList.css';

import List from '../item/ReportItem.jsx';

class ReportList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reports: null
    }
  }

  componentDidMount() {
    this.getReport()
  }

  getReport = () => {
    // eslint-disable-next-line
    var report = Array()
    firebase.database().ref().child('reports')
    .once('value').then((snap) => {
      if (snap.val()) {
        Object.keys(snap.val()).map(key => {
          const item = Object.assign({}, snap.val()[key], {id: key})
          report.push(item)
          return (null)
        })
      }
    })
    .then(() => {
      this.setState({reports: report})
    })
  }

  render() {
    return (
      <div className="report-list-container">
        <div className="report-list-border">
          <h4 className="report-list-logo"><strong>Report List</strong></h4>
          <div className="report-list-area mt-4">
            {
              this.state.reports ? 
              ( 
                this.state.reports.length > 0 ?
                (
                  Object.values(this.state.reports).map((item, key) => {
                    return (
                      <List key={key} report={item} />
                    )
                  })
                ) :
                (null)
              ) : 
              (null)
            }
          </div>
        </div>
      </div>
    );
  }
}

export default ReportList;