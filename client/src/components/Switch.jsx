import React, { Component } from 'react';
import firebase from 'firebase';
import axios from 'axios';
import * as validate from '../libs/validate';
import { path } from '../resources/DataSet';
import CircleBar from '../resources/images/waiting-circle/waiting_bar.gif';

class Switch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: null
    }
  }

  componentDidMount() {
    localStorage.removeItem('SideBar');
    this.listenServer();
    this.getChangePage();
  }

  listenServer = () => {
    axios.get(`${path}/starts`)
    .then(res => {
      console.log(res.data);
    })
  }

  getChangePage() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({user: user}, () => {
          firebase.database().ref().child('users').child(this.state.user.uid).once('value')
          .then((snap) => {
            if (snap.val()) {
              this.setState({
                role: snap.val().user_type,
              }, () => {
                if (validate.equalsIgnoreCase(this.state.role, 'ADMIN')) {
                  window.location.replace('/admin/user_list');
                } else if (validate.equalsIgnoreCase(this.state.role, 'USER')) {
                  window.location.replace('/cloud/upload');
                } else if (validate.equalsIgnoreCase(this.state.role, 'SERVICE')) {
                  localStorage.setItem('type', 'WAIT_PAYMENT');
                  window.location.replace('/service/accept/list');
                } else {
                  window.location.replace('/login');
                }
              });
            }
          })
        });
      } else {
        window.location.replace('/login');
      }
    });
  }

  render() {
    return (
      <div className="switch-circle">
        <img src={CircleBar} alt="circle" width="100" height="100"/>
      </div>
    );
  }
}

export default Switch;