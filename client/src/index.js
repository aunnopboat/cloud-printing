import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import firebase from 'firebase';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

var config = {
  apiKey: "AIzaSyA9XUsblfob5MMlGUqMeISBq_CKrbj7P2g",
  authDomain: "cloud-printing-project-669a3.firebaseapp.com",
  databaseURL: "https://cloud-printing-project-669a3.firebaseio.com",
  projectId: "cloud-printing-project-669a3",
  storageBucket: "cloud-printing-project-669a3.appspot.com",
  messagingSenderId: "816093340825"
};
firebase.initializeApp(config);

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
