import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Wait from './components/Switch.jsx';
import Account from './components/account/Account.jsx';
import Login from './components/auth/login/Login.jsx';
import Register from './components/auth/register/Register.jsx';
import MapView from './components/system/map/MapView.jsx';
import SendRequest from './components/system/send-report/SendReport.jsx';
import Upload from './components/user/upload/Upload.jsx';
import FileList from './components/user/file/list/FileList.jsx';
import StatusList from './components/user/status/list/StatusList.jsx';
import Incomes from './components/service/incomes/ServiceIncome.jsx';
import Score from './components/service/score/ServiceScore.jsx';
import AcceptList from './components/service/request-accept/list/AcceptList.jsx';
import UnAcceptList from './components/service/request-pending/list/UnAcceptList.jsx';
import UserList from './components/admin/user-management/list/UserList.jsx';
import ReportList from './components/admin/system-report/list/ReportList.jsx';
import ServiceIncomeRate from './components/admin/service-income-rate/ServiceIncomeRate.jsx';

class Router extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Wait} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/cloud/user_account" component={Account} />
        <Route exact path="/cloud/upload" component={Upload} />
        <Route exact path="/cloud/map" component={MapView} />
        <Route exact path="/cloud/send/report" component={SendRequest} />
        <Route exact path="/cloud/user/file" component={FileList} />
        <Route exact path="/cloud/request/status" component={StatusList} />
        <Route exact path="/service/incomes" component={Incomes} />
        <Route exact path="/service/accept/list" component={AcceptList} />
        <Route exact path="/service/request/pending/list" component={UnAcceptList} />
        <Route exact path="/service/score" component={Score} />
        <Route exact path="/admin/user_list" component={UserList} />
        <Route exact path="/admin/report/list" component={ReportList} />
        <Route exact path="/admin/income_rate" component={ServiceIncomeRate} />
      </Switch>
    );
  }
}
  
export default Router;