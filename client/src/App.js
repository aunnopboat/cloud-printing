import React, { Component } from 'react';
import './App.css';

import Router from './router';
import Sidebar from './components/navbar/Sidebar.jsx';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router />
        <Sidebar />
      </div>
    );
  }
}

export default App;
