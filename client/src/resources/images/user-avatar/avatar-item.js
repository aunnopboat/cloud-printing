import avatarNull from './avatar-icon-null.jpg';
import avatar1 from './avatar-icon-1.jpg';
import avatar2 from './avatar-icon-2.jpg';
import avatar3 from './avatar-icon-3.jpg';
import avatar4 from './avatar-icon-4.jpg';

export const AVATAR = [avatarNull, avatar1, avatar2, avatar3, avatar4];