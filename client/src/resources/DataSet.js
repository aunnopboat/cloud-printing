export const path = `http://localhost:5000`

export const MONTH = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

export const CHART_COLOR = [
  '#36a2eb99', 
  '#ffce5699', 
  '#4bc0c099', 
  '#9966ff99', 
  '#9966ff99', 
  '#ff638499', 
  '#9966ff99', 
  '#9966ff99', 
  '#4bc0c099', 
  '#ffce5699', 
  '#36a2eb99'
];